/*
$ Checkstyle lekriss.java #linting
$ javac lekriss.java #Compilation
*/
import java.util.Scanner;
import java.lang.Math;
import java.io.File;

class lekriss {

  public static void main (String args[]) throws Exception {

    File file = new File("DATA.lst");
    Scanner input = new Scanner(file);
    int x = input.nextInt();
    String result = "";

    if (x<2)
      System.out.println("0");
    if (x==2)
      System.out.println("2");
    for (int i = 2; i<x; i++){
      int contador = 0;
      for (int j = 2; j<i; j++) {
        if (i%j==0) {
          contador ++;
        }
      }
      if (contador==0) {
        result += i + " ";
      }
    }
    System.out.println(result);
  }
}
/*
$ java lekriss
$ 2 3 5 7
*/
