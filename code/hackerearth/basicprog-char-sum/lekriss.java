/*
$ Checkstyle lekriss.java #linting
$ javac lekriss.java #Compilation
*/
import java.util.Scanner;
import java.io.File;

class lekriss {

  public static void main (String args[]) throws Exception{

    File file = new File("Data.lst");
    Scanner input = new Scanner(file);
    int sum = 0;
    String toSum = input.nextLine();

    for(int i=0; i<toSum.length(); i++) {
      sum += (int)toSum.charAt(i) - 96;
    }

    System.out.println(sum+"");
  }
}
/*
$ java lekriss
$ 4
*/
