# !usr/bin/python
# [Finished in 0.0s]

# pylint edr3mo.py
# No config file found, using default configuration

# -------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 8.57/10, +1.43)
"""
Given an string length such as "<<>>", and a strong
return a new string where the word is in the middle
of the first string, e.g. <<Yay>>
"""


def make_out_word():
    """Python string problems with no loops"""
    data_file = open("DATA.lst", "r")
    data_set = data_file.read().split(" ")
    string_1 = data_set[0]
    string_2 = data_set[1]
    print string_1[0:2]+string_2+string_1[2:5]


make_out_word()
# python -/edr3mo.py
# **word**
