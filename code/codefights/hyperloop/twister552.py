hyperloop = f = lambda a, b, t, s=0, c=[]: a == b and s or min(
    [9999] + [f(y, b, t, s + int(z), c + [a]) for x, y, z in t if x == a and not y in c])
