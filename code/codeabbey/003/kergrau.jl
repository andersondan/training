#=
 $julia
 julia> using Lint
 julia> lintfile("kergrau.jl")
=#

open("DATA.lst") do file
  flag = false
  for ln in eachline(file)
    answer = 0
    number = 0
    if flag == false
      flag = true
      continue
    end
    for i in split(ln, " ")
      number = parse(Int64, i)
      answer = answer + number
    end
    println("$answer ")
  end
end

# $julia kergrau.jl
# 602139 543855 1409727 707784 745831 738766 1493476 792403 881291 811859
# 732132 1458154 1016464 1525208 666583
