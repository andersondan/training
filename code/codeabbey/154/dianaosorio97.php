<?php
/**
 *
 * $ phpcs dianaosorio97.php #linting
 * $ php dianaosorio97.php  #Compilation
 *
*/

if (file_exists('DATA.lst')) {
  $file = fopen("DATA.lst", "r");
  $numbers = explode(' ', fgets($file, 128));
  $nodes = $numbers[0];
  $edges = $numbers[1];
  $graph = [];
  $queue = new SplQueue();
  $seen = [];
  $queue->enqueue(0);
  array_push($seen, 0);
  $tuple = [];
  $neighbors = [];
  $answer = array(0=>-1);
  for ($i=0; $i < $edges; $i++) {
    $vertex = array_map('intval', explode(' ', fgets($file, 128)));
    array_push($graph, $vertex);
  }
  while ($queue->count() > 0) {
    $nodeCurrent = $queue->dequeue();
    foreach ($graph as $child) {
      if (in_array($nodeCurrent, $child)) {
        $tuple[] = $child;
      }
    }
    foreach ($tuple as $k) {
      $index = array_search($nodeCurrent, $k);
      unset($k[$index]);
      array_push($neighbors, $k);
    }
    $neighbors = array_merge(...$neighbors);
    sort($neighbors);
    foreach ($neighbors as $k) {
      if (in_array($k, $seen)==0) {
        array_push($seen, $k);
        $queue->enqueue($k);
        $answer[$k] = $nodeCurrent;
      }
    }
    $tuple = [];
    $neighbors = [];
  }
  for ($i=0; $i < count($answer); $i++) {
    echo($answer[$i])." ";
  }
  echo "\n";
} else {
    echo "Fail";
}
/*
php dianaosorio97.php
output:
-1 12 39 13 0 2 27 27 34 13 4 21 0 0 0 13 10 1 12
32 34 14 3 2 39 13 14 4 3 12 10 9 12 1 4 39 27 27 4 0
*/
?>
