let t =(Scanf.scanf "%d\n"(fun t ->  t));;

let printArrayFloat r = Array.iter (Printf.printf "%F ") r;;

let u x1 y1 x2 y2 xp yp= let r = ref 0.0 in
                        let a = (((xp-.x1) *. (x2-.x1)) +. ((yp-.y1) *. (y2-.y1))) in
                        let b = ((x2-.x1) *. (x2-.x1)) +. ((y2-.y1) *. (y2-.y1)) in
                        r := a /. b ;
                        !r;;

let distPoint x1 y1 x2 y2 = let r = ref 0.0 in
                            let a = (x2 -. x1) *. (x2 -. x1) in
                            let b = (y2-.y1) *. (y2-.y1) in
                            r := sqrt ( a +. b);
                            !r;;

let px x1 x2 u = x1 +. u *. (x2 -. x1);;
let py y1 y2 u = y1 +. u *. (y2 -. y1);;
let result x1 y1 x2 y2 xp yp val_u val_px val_py = let r = ref 0.0 in
                                    if  ( val_u >= 0.0) && (val_u < 1.0) then
                                        r := distPoint val_px val_py xp yp
                                    else if val_u >= 1.0 then
                                        r := distPoint x2 y2 xp yp
                                        else r := distPoint x1 y1 xp yp;
                                    !r;;

let res = Array.create_float t;;
for i=0 to t-1 do
    let values = read_line () in
    let intlist = List.map int_of_string(Str.split (Str.regexp " ") values) in
    let x1 = float (List.nth intlist 0) in
    let y1 = float (List.nth intlist 1) in
    let x2 = float (List.nth intlist 2) in
    let y2 = float (List.nth intlist 3) in
    let xp = float (List.nth intlist 4) in
    let yp = float (List.nth intlist 5) in
    let val_u = u x1 y1 x2 y2 xp yp in
    let val_px = px x1 x2 val_u in
    let val_py = py y1 y2 val_u in
    let val_r = result x1 y1 x2 y2 xp yp val_u val_px val_py in
    res.(i) <- val_r
done;
printArrayFloat res;;
