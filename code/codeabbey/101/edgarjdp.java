/**
 * $ javac edgarjdp.java
 * $ pmd -d edgarjdp.java -f text -R rulesets/java/quickstart.xml -version 1.5
 * \-language java -debug
 * $
 */

package gradientcalculation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class GradientCalculation {
  public static void main(String[] args) throws FileNotFoundException,
  IOException{
  double a=0.3;
  double b=-0.6;
  double c=8;
  double delta=1e-9;
  String str=" ";
  String ln=str;
  String[] data=new String[2];
  Boolean f=false;
  FileReader fr=new FileReader("DATA.lst"); //Load file in memory
  try (BufferedReader buffer = new BufferedReader(fr)) {
    ln=buffer.readLine();
    while (ln!=null) {   //Read line on line
      if(f){
        if(ln.contains(str)){
          data=ln.split(str);
          //Calculation of f
          double fnc=Math.pow(Double.parseDouble(data[0])-a,2)
          +Math.pow(Double.parseDouble(data[1])-b,2)
          +c*Math.exp(-Math.pow(Double.parseDouble(data[0])+a,2)
          -Math.pow(Double.parseDouble(data[1])+b,2));
          //Calculation of fdx
          double fdx=Math.pow((Double.parseDouble(data[0])+delta)-a,2)
          +Math.pow(Double.parseDouble(data[1])-b,2)
          +c*Math.exp(-Math.pow((Double.parseDouble(data[0])+delta)+a,2)
          -Math.pow(Double.parseDouble(data[1])+b,2));
          //Calculation of fdy
          double fdy=Math.pow(Double.parseDouble(data[0])-a,2)
          +Math.pow((Double.parseDouble(data[1])+delta)-b,2)
          +c*Math.exp(-Math.pow(Double.parseDouble(data[0])+a,2)
          -Math.pow((Double.parseDouble(data[1])+delta)+b,2));
          //gradient x-component
          double gx0=(fdx-fnc)/delta;
          //gradient y-component
          double gy0=(fdy-fnc)/delta;
          //Direction of gradient
          double angle=180+Math.atan2(gy0, gx0)*180/Math.PI;
          //Showing results
          System.out.println(Math.round(angle)+" ");
        }
      }
      ln=buffer.readLine();
      f=true;
    }
  }
 }
}

/**
 * $ java edgarjdp
 * $ 316 333 295 262 281 347 312 173 318 301 258 168 302 297 282
*/
