#!/bin/bash
#
# Problem #18 Square Root
#
while read -r line || [[ -n "$line" ]];
do
    array=($line)
    len="${#array[@]}"
    count=0
    if [[ "$len" -gt 1 ]];
    then
        X="${array[0]}"
        N="${array[1]}"
        r=1
        if [[ "$N" -eq "0" ]];
        then
            r=1
            total[$count]="$r"
        else
            while [[ "$N" -gt 0 ]]
            do
                d=$(echo "scale=7; $X/$r" | bc)

                r=$(echo "scale=7; $r+$d" | bc)
                r=$(echo "scale=7; $r"/2 | bc)

                N=$(echo "scale=7; $N"-1 | bc)
            done

            total[$count]="$r"
        fi
        let "count+=1"
    fi

    printf '%s ' "${total[@]}"
done < "$1" 
