from math import sqrt
data = [x.split() for x in """55 13
37 13
487 9
86153 11
30 6
6586 12
3561 12
8047 11
69386 5
758 10
82 3
83 7
24 13""".splitlines()]

def HeronSqr(p, n):
    x = 1
    xr = sqrt(p)
    for i in range(1, n):
        x = 0.5 * (x + p / x)
    x = round(x, 12 - len(str(int(x))))
    print(x, end=" ")

for array in data:
    HeronSqr(int(array[0]), int(array[1]) + 1)
