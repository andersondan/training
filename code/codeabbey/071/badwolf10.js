/*
$ eslint badwolf10.js #linting
$ node badwolf10.js #compilation
*/

/* eslint-disable filenames/match-regex */

/* Recursion over large values produces an stack overflow error, in order to
prevent it there is something called Tail Call Optimization.
TCO clears the execution stack on each recursive call*/
const tcoopt = require('tco');

const fibonacci = tcoopt(
  (fn1, fn2, index, mval) => {
    if ((fn1 + fn2) % mval === 0) {
      // necessary null return for tail call optimization module
      // eslint-disable-next-line fp/no-nil
      return [ null, index ];
    }
    const fibn = (fn1 + fn2) % mval;
    return [ fibonacci, [ fibn, fn1, index + 1, mval ] ];
  }
);

function getfiboindex(mval) {
  return fibonacci(1, 0, 2, mval);
}

// some functions do not need to return a value
/* eslint-disable fp/no-unused-expression*/
function findIndex(readerr, contents) {
  if (readerr) {
    return readerr;
  }
  const dataLines = contents.split('\n');
  const numbers = dataLines[1].split(' ').map(Number);
  const findex = numbers.map(getfiboindex);
  // eslint-disable-next-line no-console
  findex.map((x) => console.log(x));
  return 0;
}

const filesys = require('fs');
function main() {
  return filesys.readFile('DATA.lst', 'utf8', (readerr, contents) =>
    findIndex(readerr, contents));
}

main();
/*
$ node badwolf10.js
71100
123000
1400
28290
60414
233172
3480
857744
54420
320630
29160
1015890
118608
73308
55836
12654
101610
172533
68100
*/
