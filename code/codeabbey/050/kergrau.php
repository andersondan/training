<?php
/**
 * This is a challenge 50 from codeabbey.
 *
 * PHP version 7.0.32
 *
 * @category Challenges
 * @package  Palindrome
 * @author   Kerley Grau <kergrau@hotmail.com>
 * @license  https://creativecommons.org/ Creative Commons
 * @link     none
 * phpcs kergrau.php
*/

$answer = "";
$flag = false;
$c = 0;
$minor = 0;

$file = fopen("DATA.lst", "r") or exit("Unable to open file!");

while (!feof($file)) {

    $line = fgets($file);

    if ($flag == false) {
        $flag = true;
        $minor = (int)$line;
        continue;
    }

    $right = strtolower(preg_replace('/\s+/', '', $line));
    $right = preg_replace('/[^a-z]/', '', $right);
    $reverse = strrev($right);

    if (strcmp($right, $reverse) == 0) {
        $answer = $answer . "Y ";
    }
    if (strcmp($right, $reverse) != 0) {
        $answer = $answer . "N ";
    }

    $c += 1;

    if ($c == $minor) {
        break;
    }
}

echo $answer;
fclose($file);

// php kergrau.php
// N Y Y
?>
