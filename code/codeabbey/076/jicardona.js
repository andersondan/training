#!/usr/bin/env jsc
/*
$ eslint jicardona.js
*/

/* global arguments, print */

const [ input ] = arguments;
const testCases = input.split(/\n/);
const total = testCases.shift();

const answer = [];

/* eslint-disable array-element-newline */
const board = [
  [ 'R', 'N', 'B', 'Q', 'K', 'B', 'N', 'R' ],
  [ 'P', 'P', 'P', 'P', 'P', 'P', 'P', 'P' ],
  [ '-', '-', '-', '-', '-', '-', '-', '-' ],
  [ '-', '-', '-', '-', '-', '-', '-', '-' ],
  [ '-', '-', '-', '-', '-', '-', '-', '-' ],
  [ '-', '-', '-', '-', '-', '-', '-', '-' ],
  [ 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p' ],
  [ 'r', 'n', 'b', 'q', 'k', 'b', 'n', 'r' ],
];
/* eslint-enable array-element-newline */

const base = 97;
const white = [ '-', 'P', 'N', 'B', 'R', 'Q', 'K' ];
const lower = 1;
const black = [ '-', 'p', 'n', 'b', 'r', 'q', 'k' ];
const upper = 6;

for (let testCase = 0; testCase < total; testCase++) {
  const game = [];
  for (let boxes = 0; boxes < board.length; boxes++) {
    game[boxes] = board[boxes].slice();
  }
  const moves = testCases[testCase].split(/\s/);
  let move = 0;
  for (; move < moves.length; move++) {
    const orgRow = moves[move].slice(0, 2)[1] - 1;
    const orgCol = moves[move].slice(0, 2)[0].charCodeAt() - base;
    const desRow = moves[move].slice(2)[1] - 1;
    const desCol = moves[move].slice(2)[0].charCodeAt() - base;
    const org = game[orgRow][orgCol];
    const des = game[desRow][desCol];
    if (org === 'P') {
      if (orgRow >= desRow) {
        break;
      }
      if (desRow - orgRow > 1) {
        if (orgCol !== desCol) {
          break;
        }
        if (orgRow !== lower || desRow - orgRow > 2) {
          break;
        } else if (game[desRow - 1][desCol] !== '-') {
          break;
        }
      }
    }
    if (org === 'p') {
      if (orgRow <= desRow) {
        break;
      }
      if (orgRow - desRow > 1) {
        if (orgCol !== desCol) {
          break;
        }
        if (orgRow !== upper || orgRow - desRow > 2) {
          break;
        } else if (game[desRow + 1][desCol] !== '-') {
          break;
        }
      }
    }
    if (orgCol !== desCol) {
      if ((org === 'P' && white.includes(des)) ||
          (org === 'p' && black.includes(des))) {
        break;
      }
    } else if (des !== '-') {
      break;
    }
    game[desRow][desCol] = org;
    game[orgRow][orgCol] = '-';
  }
  answer.push(move === moves.length ? 0 : move + 1);
}

print(answer.join(' '));

/*
jsc jicardona.js -- "`cat DATA.lst`"
6 4 0 0 6 0 6 4 4 3 4 4 2
*/
