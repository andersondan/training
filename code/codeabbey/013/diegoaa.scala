/*
$ java -jar scalastyle_2.12-1.0.0-batch.jar --config scalastyle-config.xml
diegoaa.scala #linting
Processed 1 file(s)
Found 0 errors
Found 0 warnings
Finished in 592 ms
$ scalac Diegoaa.scala #compilation
*/

import scala.io.Source
object Diegoaa extends App {
  val source = Source.fromFile("DATA.lst").getLines.toArray
  var data = source(1)
  var newData = data.split(" ")
  newData.foreach(e => wsd(e))

  def wsd(l: String) = {
    var sum = 0
    var i = 1
    l.split("").foreach {
      e =>
        sum = sum + (e.toInt * (i))
        i = i + 1
    }
    print(s"$sum ")
  }
}

/*
$ scala Diegoaa
38 118 253 154 27 15 139 8 2 38 34 60 21 6 138 113 128 204 179 128 8 224
46 1 40 172 68 110 31 142 26 15 37 1 170 9
*/
