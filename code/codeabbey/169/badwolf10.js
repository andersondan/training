/*
$ eslint badwolf10.js #linting
$ node badwolf10.js #compilation
*/

// Unnecesary or conflicting rules
/* eslint-disable filenames/match-regex */
/* eslint-disable fp/no-unused-expression */
/* eslint-disable no-console */
/* eslint-disable array-bracket-newline */
/* eslint-disable array-element-newline */

// Simulation params
const moonradius = 1737100;
const surfgravity = 1.622;
const Vexhaust = 2800;
const maxbrate = 100;
const burntimech = 1;
const steps = 100;

// Optimization params
const optsteps = 1000;
const draterndstepsize = 0.0001;
const maxinit = 100;
const ivel = 5000;
const minvel = 5;
const firstguessv = 300;

const rndint = require('random-int');
const tco = require('tco');

function itimerndstep() {
  return rndint(0 - 1, 1);
}

function ratezrndstep() {
  return rndint(-maxinit, maxinit) / maxinit;
}

function dratezrndstep() {
  return (2 * Math.random() * draterndstepsize) - draterndstepsize;
}

const performSteps = tco((craftparams, burnrateinit, nsteps) => {
  if ((nsteps === 0) || (craftparams[2] <= 0)) {
    // eslint-disable-next-line fp/no-nil
    return [ null, craftparams ];
  }
  const [ craftmass, fuelmassread, height, velocity ] = craftparams;
  const deltaT = burntimech / steps;
  const fuelmass = Math.max(fuelmassread, 0);
  const nheight = height - (velocity * deltaT);
  const burnrate = fuelmass === 0 ? 0 : burnrateinit;
  const deltaM = burnrate * deltaT;
  const deltaV = Vexhaust * deltaM / (craftmass + fuelmass);
  const nfuelmass = fuelmass - deltaM;
  const currgravity = surfgravity * (Math.pow(moonradius, 2) /
    Math.pow(moonradius + nheight, 2));
  const nvelocity = velocity + (currgravity * deltaT) - deltaV;
  return [ performSteps,
    [ [ craftmass, nfuelmass, nheight, nvelocity ], burnrate, nsteps - 1 ],
  ];
});

const craftLandingSystem = tco((ctrlparm, craftparams, currtime) => {
  if (craftparams[2] <= 0) {
    // Landed
    // eslint-disable-next-line fp/no-nil
    return [ null, craftparams[1 + 1 + 1] ];
  }
  if (currtime < ctrlparm[0] || craftparams[1] <= 0) {
    // In initial free fall or out of fuel
    const ncraftparams = performSteps(craftparams, 0, steps);
    return [ craftLandingSystem, [ ctrlparm, ncraftparams, currtime + 1 ] ];
  }
  const brate = ctrlparm[1] + (ctrlparm[2] * (currtime - ctrlparm[0]));
  const burnrate = Math.min(Math.max(brate, 0), maxbrate);
  const ncraftparams = performSteps(craftparams, burnrate, steps);
  return [ craftLandingSystem, [ ctrlparm, ncraftparams, currtime + 1 ] ];
});

const goDeep = tco((craftparams, ctrlparam, rndchng, vel) => {
  const nctrlparam = ctrlparam.map((par, indx) => {
    if (indx === 0 || indx === 1) {
      if (par + rndchng[indx] >= 0 && par + rndchng[indx] <= maxinit) {
        return par + rndchng[indx];
      }
      return rndint(maxinit);
    }
    return par + rndchng[indx];
  });
  const landvel = craftLandingSystem(nctrlparam, craftparams, 0);
  if (landvel > vel) {
    // eslint-disable-next-line fp/no-nil
    return [ null, [ ctrlparam, vel ] ];
  }
  return [ goDeep, [ craftparams, nctrlparam, rndchng, landvel ] ];
});

const searchParams = tco((craftparams, ctrlparm, velocity, iterations) => {
  if (velocity < minvel || iterations <= 0) {
    // eslint-disable-next-line fp/no-nil
    return [ null, [ ctrlparm, velocity ] ];
  }
  const [ cparams, vel ] = goDeep(craftparams, ctrlparm,
    [ itimerndstep(), ratezrndstep(), dratezrndstep() ], velocity);
  return [ searchParams, [ craftparams, cparams, vel, iterations - 1 ] ];
});

const optimizeParams = tco((craftparams, ctrlparam) => {
  if (craftLandingSystem(ctrlparam, craftparams, 0) > firstguessv) {
    return [ optimizeParams,
      [ craftparams, [ rndint(maxinit), rndint(maxinit), 0 ] ] ];
  }
  const [ optparam, nvel ] = searchParams(craftparams, ctrlparam, ivel,
    optsteps);
  console.log(`velocity found: ${ nvel }`);
  if (nvel < minvel) {
    // eslint-disable-next-line fp/no-nil
    return [ null, optparam ];
  }
  return [ optimizeParams,
    [ craftparams, [ rndint(maxinit), rndint(maxinit), 0 ] ] ];
});

function automateLanding(readerr, contents) {
  if (readerr) {
    return readerr;
  }
  const dataLines = contents.split('\n');
  const initparams = dataLines.slice(1).map((row) =>
    row.split(' ').map(Number));
  const optparams = initparams.map((params) =>
    optimizeParams(params, [ rndint(maxinit), rndint(maxinit), 0 ]));
  optparams.map((par) => par.map((pari, indx) => {
    if (indx === 1) {
      console.log(pari.toFixed(2));
    } else {
      console.log(pari);
    }
    return 0;
  }));
  return 0;
}

const filesys = require('fs');
function main() {
  return filesys.readFile('DATA.lst', 'utf8', (readerr, contents) =>
    automateLanding(readerr, contents));
}

main();

/*
$ node badwolf10.js
28
17.06
0.00037155612323193154
97
59.35
0.00046365892490620727
82
42.15
0.0000882524516071044
66
19.61
0.00045645932849113337
91
75.00
0.0005191366136585051
*/
