#!/usr/bin/env jsc
/*
$ eslint jicardona.js
*/

/* global print */

const [ input ] = arguments;
const testCases = input.split('\n');
const total = testCases.shift();

const answer = [];

for (let testCase = 0; testCase < total; testCase++) {
  const printer = testCases[testCase].split(' ').map(Number);
  let xTime = 0;
  let yTime = 0;
  for (let page = 0; page < printer[2]; page++) {
    if (xTime + printer[0] < yTime + printer[1]) {
      xTime += printer[0];
    } else {
      yTime += printer[1];
    }
  }
  answer.push(xTime > yTime ? xTime : yTime);
}

print(answer.join(' '));

/*
$ jsc jicardona.js -- "`cat DATA.lst`"
318244719 13158054 278833660 132435810 51272717  46729010  8760609 107074208
309055713 33295378 322580664 359769725 332892224 162592318 175123025
*/
