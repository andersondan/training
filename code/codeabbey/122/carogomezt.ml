let t =(Scanf.scanf "%d\n"(fun t ->  t));;

let partition arr left right = let lt = ref left in
                                let rt = ref right in
                                let dir = ref "left" in
                                let pivot = ref arr.(left) in
                                while !lt < !rt do
                                    if !dir = "left" then
                                    (
                                        if arr.(!rt) > !pivot then
                                        (
                                            rt := !rt - 1;
                                        )
                                        else (
                                            arr.(!lt) <- arr.(!rt);
                                            lt := !lt + 1;
                                            dir := "right"
                                        )
                                    )
                                    else (
                                        if arr.(!lt) < !pivot then
                                        (
                                            lt := !lt + 1;
                                        )
                                        else (
                                            arr.(!rt) <- arr.(!lt);
                                            rt := !rt - 1;
                                            dir := "left"
                                        )
                                    )
                                done;
                                arr.(!lt) <- !pivot;
                                !lt;;


let rec quicksort arr left right = let pivot_pos = (partition arr left right) in
                                if pivot_pos - left > 1 then
                                begin
                                    Printf.printf "%d%s%d " left "-" (pivot_pos -1);
                                    quicksort arr left (pivot_pos - 1)
                                end;
                                if right - pivot_pos > 1 then
                                begin
                                    Printf.printf "%d%s%d " (pivot_pos + 1) "-" right;
                                    quicksort arr (pivot_pos + 1) right
                                end;;

let printArray r = Array.iter (Printf.printf "%d ") r;;

let values = read_line () in
let intList = List.map int_of_string(Str.split (Str.regexp " ") values) in

let intArray = Array.of_list intList in
let posEnd = (Array.length intArray) - 1 in

Printf.printf "%d%s%d " 0 "-" posEnd;
quicksort intArray 0 posEnd;;
