###
$ coffeelint --reporter jslint danmur97.coffee #linting
$ coffee -c danmur97.coffee #compilation
###

length_def = false # bool #
data_length = 0 # int #
data_index = 0 # int #
data = [] # Array #

#   purpose
#     Shows list of results of collatz_counter for each suplied input
main = (x)->
  data_length = x[2]
  data = x[3].split " "
  console.log(collatz_counter(data[i])) for i in [0...data_length]
  0

#   purpose
#     Count steps for making input(x) go to 1, with collatz_step algorithm
#   params
#     x: input integer
collatz_counter = (x)->
  steps = 0
  y = x
  while(y != 1)
    y = collatz_step(y)
    steps++
  steps

#   purpose
#     Implement collatz algorithm for one step
#   params
#     x: input integer
collatz_step = (x)->
  r = 0
  if(x % 2 == 0)
    r = x/2
  else
    r = (3*x+1)
  r
main(process.argv)

###
$ coffee danmur97.coffee (cat DATA.lst)
106 9 24 44 11 104 22 73 64 29 10 26 45 40 20 111 187 126 23 70 11 45 21
###
