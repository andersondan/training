; $ lein check
;   Compiling namespace kedavamaru.clj
; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting kedavamaru.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

; namespace
(ns kedavamaru.clj
  (:gen-class)
)

; tokenize whitespace separated string into a vector of integers
(defn stoi
  ([string]
    (map #(Integer/parseInt %)
      (clojure.string/split string #" ")
    )
  )
)

; get seconds since date 0 0 0 0
(defn getalls
  ([d h m s]
    (let [r   0
          r   (+ r s)
          r   (+ r (* 60 m))
          r   (+ r (* 3600 h))
          r   (+ r (* 86400 d))]
      r
    )
  )
)

; parse file line by line and solve
(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [l (stoi line)
              v (into [] l)]
          (if (= 8 (count l))
            (let [
                  ; initial date
                  d1 (get v 0)
                  h1 (get v 1)
                  m1 (get v 2)
                  s1 (get v 3)

                  ; final date
                  d2 (get v 4)
                  h2 (get v 5)
                  m2 (get v 6)
                  s2 (get v 7)

                  ; buffer to flush
                  bf (- (getalls d2 h2 m2 s2) (getalls d1 h1 m1 s1))

                  ; start flushing
                  fd (int (/ bf 86400))
                  bf (mod bf 86400)
                  fh (int (/ bf 3600))
                  bf (mod bf 3600)
                  fm (int (/ bf 60))
                  bf (mod bf 60)
                  fs (int bf)]
              (print (str "(" fd " " fh " " fm " " fs ") "))
            )
          )
        )
      )
    )
  )
)

; execute
(defn -main
  ([& args]
    (process_file "DATA.lst")
    (println)
  )
)

; $lein run
;   (17 19 1 41) (17 10 23 45) (15 7 59 4) (6 5 48 34) (0 8 47 27) (2 2 9 6)
;   (17 5 13 33) (11 19 37 8) (5 3 49 34) (0 0 2 4)
