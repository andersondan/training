clc
clear all
close all

x_hands=input('type how many hands to evaluate: ','s');
x_hands = str2double(x_hands);
k = 0;
while k < x_hands
    n_hands=input('Enter the hand combination (Letter must be in Capital and separate by space in between): ','s');
    disp('The hands of the players')
    hands =regexp(n_hands,',','split');
    disp(hands)
    n = size(n_hands,1);
    %identify the numbers & letters
    exp_num = '[\d*\.]*\d*';
    temp_num = regexp(hands,exp_num,'match');

    num_str = [temp_num{:}];
    n_numb = size(num_str,2);
    res_num = [];
    for j = 1 : n_numb
        v_j = str2double(num_str{j});
        res_num = [res_num, v_j]; %suma de numeros por mano

    end
    sum_num = sum(res_num);

    exp_let = 'A|K|Q|J|T';
    let = regexp(n_hands, exp_let, 'match');
    n_let = size(let,2);
    sum_let = [];
    As = false;
    for i = 1 : n_let
        y_i = let{i};
        switch y_i
            case 'T'
                x_i = 10;
                sum_let = [sum_let, x_i];
            case 'J'
                x_i = 10;
                sum_let = [sum_let, x_i];
            case 'Q'
                x_i = 10;
                sum_let = [sum_let, x_i];
            case 'K'
                x_i = 10;
                sum_let = [sum_let, x_i];
            case 'A'
                x_i = 11;
                sum_let = [sum_let, x_i];
                As = true;
        end
    end
    sum_let = sum(sum_let);

    sum_tot = sum_let + sum_num;

    if sum_tot > 21 & As == true
        sum_tot = sum_tot - 10;
        As = false;
    end
    if sum_tot > 21
        disp('Bust')
    end
    if sum_tot <= 21
        disp('Sum cards of the Hand:')
        disp(sum_tot)
    end
    k = k + 1;
end
