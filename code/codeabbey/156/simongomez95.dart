/*
➜  156 git:(sgomezatfluid) ✗ dartanalyzer sgomezatfluid.dart
Analyzing sgomezatfluid.dart...
No issues found!
*/

import 'dart:io';

void main() async {
  List<String> data = await new File('DATA.lst').readAsLines();
  String answer = '';
  data.forEach(
    (card) {
      List csumret = checkSum(card);
      int csum = csumret[0];
      bool isMissing = csumret[1];
      int placeMissing = csumret[2];
      String corrected = isMissing
          ? correctMissing(card, placeMissing).toString()
          : correctSwapped(card).toString();
      answer += corrected + ' ';
    }
  );
  print(answer);
}

int correctSwapped(String card) {
  int ret;
  for(int i=card.length-1; i >= 0; i--) {
    if(i > 0) {
      String swap = swapWithPrev(card, i);
      if(checkSum(swap)[0]%10 == 0) {
        ret = int.parse(swap);
      }
    }

  }
  return ret;
}

String swapWithPrev(String str, int index) {
  List<String> strlist = str.split('');
  String a = strlist[index];
  String b = strlist[index-1];
  strlist[index] = b;
  strlist[index-1] = a;
  return strlist.join();
}

int correctMissing(String card, int place) {
  int sum = checkSum(card)[0];
  int nextCorrect = (sum/10).ceil() * 10;
  int difference = nextCorrect - sum;
  double missingNumber;
  if(place%2 > 0) {
    missingNumber = difference.toDouble();
  } else {
    missingNumber = difference%2 > 0 ? (difference+9)/2 : difference/2;
  }
  card = replaceCharAt(card, place, missingNumber.toInt().toString());
  return int.parse(card);
}

List checkSum(String numstring) {
  bool isMissing=false;
  int missingPlace;
  int sum = 0;
  for(int i=numstring.length-1; i >= 0; i--) {
    String snum = numstring[i];
    if(snum == '?') {
      isMissing = true;
      missingPlace = i;
    } else {
      sum = sum + checkDigit(int.parse(snum), i);
    }
  }
  return [sum, isMissing, missingPlace];
}

int checkDigit(int number, int index) {
  if(index % 2 == 0) {
    return (number*2) > 9 ? number*2 - 9 : number*2;
  } else {
    return number;
  }
}

List<int> getNumbers(List<String> numberstring) {
  List<int> numberlist = [];
  numberstring.forEach(
    (number) {
      numberlist.add(int.parse(number));
    }
  );
  return numberlist;
}

String replaceCharAt(String oldString, int index, String newChar) {
  return oldString.substring(0, index) + newChar
    + oldString.substring(index + 1);
}

/*
➜  156 git:(sgomezatfluid) ✗ dart sgomezatfluid.dart
5971406908116158 7399233746090936 8891733674480349 1493692969784665
8952895656212556 1039487771668821 9800971134974224 6320831181825561
3755352032706455 4681051690023305 9782539125121550 6042024824938036
1707272994899069 8183190141506304 1515182024139224 2293684606322561
5323786318420687 3677635982563016 5113852867840743 8724891260239025
4870574786691546 6442934446426085 6566941041650605 3831032700441662
4904706462087967 3358385699350507 3319758665633866 2816202991376189
6826872662438481 5093888508349383 8973264034398451 9715121934005225
7533864765651880 2308899351638374 1011093846615334 8536299733311629
7806698656647247 2557443471641394 4780563168993124 1414926068258502
7505966416613898
*/
