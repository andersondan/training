-- [[
-- $ luacheck skhorn.lua
-- Checking skhorn.lua                               OK
-- Total: 0 warnings / 0 errors in 1 file
-- ]]

-- luacheck: no unused args


-- Check if file exists
local function file_exists(file)
  local f = io.open(file, "rb")
  if f then f:close() end
  return f ~= nil
end

-- Get line by line of the file
local function lines_from(file)
  if not file_exists(file) then return {} end
  local lines = {}
  for line in io.lines(file) do
    lines[#lines + 1] = line
  end
  return lines
end

local function lcg(line)
  -- Split the array incoming with
  -- the 5 numbers line, and store each
  -- in an array, delimiter %S+ == space
  local line_array = {}
  local index = 1
  for item in string.gmatch(line, "%S+") do
    line_array[index] = item
    index = index + 1
  end
  -- Formula Xnext = (A * Xcur + C) % M
  local A_const = line_array[1]
  local C_const = line_array[2]
  local M_const = line_array[3]
  local X0_init = line_array[4]
  local N_last = line_array[5]

  index = N_last
  local Xnext = 0
  while true do
    if index == N_last then
      Xnext = math.floor((A_const * X0_init + C_const) % M_const)
    else
      Xnext = math.floor((A_const * Xnext + C_const) % M_const)
    end
    N_last = N_last - 1
    if N_last == 0 then break end
  end

  return Xnext
end
local file = 'DATA.lst'
local lines = lines_from(file)
local output = {}
for k,v in pairs(lines) do
  if string.len(v) ~= 2 then
    table.insert(output, lcg(v))
    -- print(lcg(v, k))
  end
end

print(table.concat(output, " "))

-- [[
-- $ lua5.3 skhorn.lua
-- 286 0 2 401907 35 30 149439 10 57 647 38566 2129 83549 13339 6
-- ]]
