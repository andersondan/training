; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting kamadoatfluid.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

; namespace
(ns kamadoatfluid.clj
  (:gen-class)
)

; tokenize whitespace separated string into a convenient type and container
(defn s2c [wsstr]
  (map #(Integer/parseInt %) (clojure.string/split wsstr #" "))
)

; return a list containing the values generated until reaching the end of a loop
(defn solve [l]
  (let [n (mod (int (/ (* (first l) (first l)) 100)) 10000)]
    (if (some #(= % n) l)
      l
      (solve (concat (list n) l))
    )
  )
)

; parse DATA.lst and solve
(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [l (s2c line) c (count l)]
          (if-not (= c 1)
            (print (map #(count (solve (list %))) l))
          )
        )
      )
    )
    (println)
  )
)

; fire it all
(defn -main [& args]
  (process_file "DATA.lst")
)

; $lein run
;   (107 105 104 100 101 110 97 103 108 103 101 106)
