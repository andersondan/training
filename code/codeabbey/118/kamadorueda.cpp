/*
Linting with CppCheck assuming the #include files are on the same
folder as kedavamaru.cpp
> cppcheck --enable=all --inconclusive --std=c++14 kedavamaru.cpp
Checking kedavamaru.cpp ...

Compiling and linking using the "Developer Command Prompt for VS 2017"
> cl /EHsc kedavamaru.cpp

Microsoft (R) C/C++ Optimizing Compiler Version 19.15.26726 for x86
Copyright (C) Microsoft Corporation.  All rights reserved.

kedavamaru.cpp

Microsoft (R) Incremental Linker Version 14.15.26726.0
Copyright (C) Microsoft Corporation.  All rights reserved.

/out:kedavamaru.exe
kedavamaru.obj
*/
/*
This problem is one of the hardest on codeabbey that is listed
on the training repository.

How is that this problem has been solved for just 35 people?

Two reasons:

The search space for this algorithm is big enough to
make infeasible a tree traversing approach and there is
literrally no feasible solution by a naive approach.

The problem asks for an score > 46000 to get in the
world rank.

That said, I performed some analisys:
- The expected value of a ramdom game using a random
approach is 0. However, due to the assymetry of the
given board values, the expected value is ~5000
- The probability of exceeding 10000 randomly is ~ 0
due to the big mapped space

Conclusion: don't traverse, don't guess, or not even
your heirs will live enough to see an output.

- The greedy approach throws a score of 35000
- From 35000 and up, increasing one point the score
increases the time complexity of finding one solution

Conclusion: Use greedy, but not too greedy, too greedy
yields an score of 35k, not greedy yields 0.

How did I get over 46000 then?

An algorithm based on a greedy approach modified with
a genetic algorithm is used.

This algorithm is almost greedy, but it introduces
some mistakes intentionally into the game route in
order to reproduce 'smart' movements, in other words
movements where you intentionally pick a value that
in the short term doesn't benefit you, but in the long
term benefits you even more, and compensates the bad
pick in the first place with an profit margin.

Since the uncertainty principle in combinatorial problems
obligues you to measure in order to know your position, the
problem is not convex, and traversing is not an option,
then is better (in time) to first get a position with the
eyes closed, and then measure what you got.

Since this technique is a little similar to an heuristic
method known as 'genetic algorithms', the vars have been
named to honor that.

Mutation rate has been calibred for the given board.

this algorithm - current score: 46328
this algorithm - world position: 19 best

best world score: 46744

this approach may beat the current best world score if
enough time is provided, but I've only run it for five
minutes

link to global rank:
http://www.codeabbey.com/index/task_chlng_stats/maxit-single-player
*/

// Calibrate the genetic algorithm
#define mutation_rate 40

// Stop when this score is achieved
#define score_objetive 46001

#include <iostream>
#include <fstream>
#include <time.h>
#include <vector>
#include <list>

using namespace std;

int getab(int a, int b) {
  int x;
  int n = b - a + 1;
  int r = RAND_MAX % n;
  do x = rand();
  while (x >= RAND_MAX - r);
  return a + x % n;
}
int getmaxpos(vector<int> const &vect) {
  int mi = 0;
  int max = vect[0];
  for (int i = 1; i < (int)vect.size(); ++i) {
    if (vect[i] > max) {
      mi = i;
      max = vect[i];
    }
  }
  return mi;
}
void printlist(list<int> const &list) {
  for (int const& e : list) cout << e << " ";
  cout << endl;
}
int verify(vector<vector<int>> board, list<int> const &moves) {
  int posx = 0;
  int posy = 0;
  int score = 0;
  bool dir = true;
  for (int const& m : moves) {
    if (dir) {
      posx = m;
      score += board[posy][posx];
      dir = false;
    }
    else {
      posy = m;
      score -= board[posy][posx];
      dir = true;
    }
    board[posy][posx] = 0;
  }
  return score;
}
list<int> getoption(vector<vector<int>> board) {
  int posx = 0;
  int posy = 0;
  list<int> moves;
  int n = board.size();

  for (;;) {
    vector<int> valid_i;
    vector<int> valid_b;

    for (int j = 0; j < n; ++j) {
      if (board[posy][j] != 0) {
        valid_i.push_back(j);
        valid_b.push_back(board[posy][j]);
      }
    }

    if (valid_i.empty()) return moves;

    if (getab(0, mutation_rate)) posx = valid_i[getmaxpos(valid_b)];
    else posx = valid_i[getab(0, valid_i.size() - 1)];

    moves.push_back(posx);
    board[posy][posx] = 0;

    valid_i.clear();
    valid_b.clear();
    for (int i = 0; i < n; ++i) {
      if (board[i][posx] != 0) {
        valid_i.push_back(i);
        valid_b.push_back(-1 * board[i][posx]);
      }
    }

    if (valid_i.empty()) return moves;

    if (getab(0, mutation_rate)) posy = valid_i[getmaxpos(valid_b)];
    else posy = valid_i[getab(0, valid_i.size() - 1)];

    moves.push_back(posy);
    board[posy][posx] = 0;
  }
}
int main() {
  fstream file("DATA.lst");
  if (!file) return 1;

  int n; file >> n;
  vector<vector<int>> board(n, vector<int>(n));
  for (int i = 0; i < n*n; ++i) file >> board[i/n][i%n];

  srand((unsigned int)time(NULL));

  list<int> answer;
  for (int i = 0, score = 0, maxscore = 0; score < score_objetive; ++i) {
    list<int> moves = getoption(board);

    score = verify(board, moves);
    if (score > maxscore) {
      answer = moves;
      maxscore = score;
    }
  }

  printlist(answer);
  file.close();
}
/*
Running using the "Windows Command Prompt", assumming "DATA.lst" is
on the same folder as kedavamaru.exe, output is non-deterministic,
the following is the output for an score of 46328

> kedavamaru.exe
28 6 1 27 15 18 21 0 4 7 5 13 23 1 22 25 4 11 13 5 7 18 2 5 21 17 3 0 22 11 18
21 14 2 22 3 2 13 4 17 10 16 7 11 20 20 17 4 25 29 5 28 4 2 29 15 2 20 28 4 3
23 25 20 5 2 10 24 24 8 13 9 5 4 21 2 0 21 6 27 11 25 27 24 4 19 14 27 7 19 28
26 7 21 13 18 1 11 21 26 6 10 18 1 21 14 13 6 14 5 6 15 23 16 16 21 24 16 20 9
9 12 8 26 3 13 11 21 21 9 15 3 28 2 18 7 11 11 15 25 20 24 13 7 16 26 23 20 14
11 17 25 8 0 2 7 20 26 29 14 14 24 0 27 12 10 10 8 25 26 0 28 25 19 0 22 14 16
9 20 22 24 29 28 2 25 5 23 15 29 7 14 16 20 24 22 3 8 12 20 13 1 1 0 26 23 20
22 9 27 25 9 26 4 2 22 27 9 4 26 12 21 27 17 14 15 4 20 8 21 4 10 2 6 9 1 6 19
19 16 22 22 13 15 7 7 6 8 9 2 6 28 14 7 25 3 10 19 12 17 29 6 8 3 4 29 8 22 6
13 7 28 21 23 23 28 18 20 1 3 18 8 20 17 25 15 12 3 26 20 19 27 16 4 23 24 19
14 3 10 24 2 15 20 6 23 0 15 24 4 6 9 24 18 4 8 14 4 13 16 8 27 23 5 25 16 26
27 2 24 6 18 27 20 21 6 18 22 23 2 11 19 13 0 6 14 23 11 8 1 3 7 27 29 10 22
28 14 17 5 0 4 29 13 16 18 20 19 9 7 24 23 7 6 19 26 11 20 7 1 19 11 29 21 10
23 1 19 27 26 24 3 23 6 12 7 21 15 26 10 19 25 24 5 22 7 28 24 15 19 3 5 20 14
12 13 19 4 9 0 16 1 5 17 7 0 11 17 18 5 16 25 26 11 10 28 8 19 23 21 26 29 18
26 1 8 11 14 27 10 17 19 5 10 7 4 15 17 28 29 14 18 17 23 13 13 8 5 19 9 18 16
29 29 11 16 21 3 14 1 2 16 0 8 7 12 14 26 2 14 9 5 5 26 13 17 23 12 13 2 19 3
16 9 7 2 2 21 15 26 26 19 22 27 20 28 28 12 6 6 11 22 26 7 23 8 17 1 25 25 12
0 29 8 2 19 21 22 25 24 18 14 15 10 29 9 11 10 13 27 4 14 5 18 19 8 28 18 0 17
26 18 10 13 27 23 12 24 21 13 18 0 27 4 1 13 28 21 5 27 21 8 26 28 22 26 17 15
11 18 18 4 12 2 26 1 29 19 18 27 3 29 0 14 8 2 16 17 6 16 4 1 11 4 10 15 20 6
15 15 28 25 7 22 1 7 29 12 5 15 27 1 12 18 25 0 23 9 3 24 7 3 0 13 14 0 24 19
16 6 25 2 17 13 26 6 17 7 8 4 22 18 9 26 10 25 18 15 19 28 16 11 27 16 28 1 20
2 27 8 15 12 21 25 14 9 8 18 23 29 6 25 3 2 1 9 10 5 28 23 8 10 1 14 26 5 27 6
22 14 25 12 0 7 19 0 17 24 16 23 11 5 12 11 2 9 17 27 24 1 0 10 14 23 29 25 23
10 20 21 17 22 15 0 10 1 15 5 4 4 20 12 18 23 22 8 8 17 1 5 29 22 4 6 3 18 29
3 17 17 2 23 9 11 6 3 27 27 28 11 3 21 9 29 20 13 22 21 1 25 9 13 25 21 19 17
9 24 26 12 11 3 3 20 10 6 0 20 29 27 10 14 24 12 22 17 24 28 1 24 8 15 22 29 2
12 12 22 19 29 16 15 9 28 11 24 5 3 20 0 5 6 24 11 25 10 21 29 13 28 17 12 4
23 19 12 10 7 15 28 27 12 16 8 5 22 16 10 22 9 28 10 9 3 13 25 0 11 5 16 3 15
1 29 12 16 15 13 24 29 17 16 1 12 3 28 12 9 0
*/
