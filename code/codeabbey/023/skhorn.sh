#!/bin/bash
: '
  skhorn@Morgul ~/D/f/t/c/c/023> shellcheck skhorn.sh 
  skhorn@Morgul ~/D/f/t/c/c/023> 
'
while read -r line || [[ -n "$line" ]];
do
  array=($line)
  
  swap=0
  i=0
  while [ $i -ne -1 ];
  do
    # Break condition. Input ends with -1
    if [[ "${array[$i]}" -eq -1 ]];
    then
      break
    fi

    # Temporal variable to store current value
    temp="${array[$i]}"
    # Temporal incrementator
    temp_i=0
    let "temp_i=$i+1"
    # Checking if next value is not -1
    if [[ "${array[$temp_i]}" -ne -1 ]];
    then
      # Checking if current value is greater than next value
      # If so, do the swap
      if [[ "$temp" -gt "${array[$temp_i]}" ]];
      then
        # Swapping process
        array[$i]="${array[$temp_i]}"
        array[$temp_i]="$temp"
        # Swap counter
        let "swap=$swap+1"
      fi
    fi
    # Counter iterator
    let "i=$i+1"
  done
  
  # Removing last value from array
  unset "array[${#array[@]}-1]"
  
  # Checksum
  LIMIT=10000007 
  result=0
  for item in "${array[@]}";
  do
    value="$item"
    result=$(echo "$result"+"$value" | bc)
    result=$(echo "$result"*113 | bc)

    if [[ "$result" -gt $LIMIT ]]
    then
      result=$(echo "$result"%"$LIMIT" | bc)
    fi
  done

  printf '%s %s' "$swap" "$result"
 
done < "$1"
: '
  skhorn@Morgul ~/D/f/t/c/c/023> ./skhorn.sh DATA.lst 
  41 1962385⏎  
'
