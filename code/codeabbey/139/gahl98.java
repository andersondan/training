/*
$ pmd -d gahl98.java -R rulesets/java/quickstart.xml -f text -min 2 #linting
May 03, 2019 8:16:40 AM net.sourceforge.pmd.PMD processFiles
WARNING: This analysis could be faster,
please consider using Incremental Analysis:
https://pmd.github.io/pmd-6.13.0/pmd_userdocs_incremental_analysis.html
$ javac -d . gahl98.java #Compilation
*/

package training.fluid;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

class Masses{
  public static int calc(Double in){
    in = in*1.0;
    int i = -1;
    int total=0;
    do {
      i++;
    } while (Math.pow(3, i)<in);
    double control = Math.pow(3, i);
    double previous = (int)Math.pow(3, i-1);
    if (control==in){
      total++;
    }else{
      if (((control+1)/2)<in){
        total++;
        total+=calc(control-in);
      }else{
        total++;
        total+=calc(in-previous);
      }
    }
    return total;
  }

  public static void main(String[] args) {
    File file = null;
    Scanner input = null;
    ArrayList<Integer> finding = null;
    try {
      file = new File("DATA.lst");
      input = new Scanner(file);
      finding = new ArrayList<Integer>();
      while (input.hasNext()) {
        finding.add(input.nextInt());
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    String sol="";
    for (int i=1; i <= finding.get(0); i++){
      sol = sol+calc(finding.get(i)*1.0)+" ";
    }
    input.nextLine();
    System.out.println(sol);
    input.close();
  }
}
/*
$ java training.fluid.Masses
9 1 11 8 7 6 7 6 12 7 3 8 10 14 12 8 6 8 4 5
*/
