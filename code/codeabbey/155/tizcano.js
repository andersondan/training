/*
$ eslint tizcano.js
$
*/

function auxNeighbors(graph, visited, start) {
  const text = graph.map((vertex) => {
    if (vertex.includes(start)) {
      if (vertex[0] === start) {
        if (!visited.includes(vertex[1])) {
          return vertex[1];
        }
      }
      if (vertex[1] === start) {
        if (!visited.includes(vertex[0])) {
          return vertex[0];
        }
      }
    }
    return false;
  });
  return text.filter((element) => element !== false);
}

function flatten(elements) {
  return elements.reduce(
    (result, current) =>
      result.concat(Array.isArray(current) ? flatten(current) : current),
    []
  );
}
function quickSort(arr) {
  if (arr.length) {
    return flatten([
      quickSort(
        arr.slice(1).filter((element) => element <= arr[0])
      ),
      arr[0],
      quickSort(
        arr.slice(1).filter((element) => element > arr[0])
      ),
    ]);
  }
  return [];
}
function compareMin(elem, pivot) {
  return elem.a <= pivot.a;
}
function compareMax(elem, pivot) {
  return pivot.a < elem.a;
}
function quickSortFn(arr, fnt, ftn) {
  if (arr.length) {
    return flatten([
      quickSortFn(
        arr.slice(1).filter((element) => fnt(element, arr[0])),
        fnt,
        ftn
      ),
      arr[0],
      quickSortFn(
        arr.slice(1).filter((element) => ftn(element, arr[0])),
        fnt,
        ftn
      ),
    ]);
  }
  return [];
}

function auxDfs(graph, visited = [], start, queue, order = []) {
  if (!queue.length) {
    return order;
  }
  if (visited.includes(start)) {
    const concatedQueue = [ ...queue ].slice(1);
    if (!concatedQueue.length) {
      return order;
    }
    return auxDfs(graph, visited, concatedQueue[0].a, concatedQueue, order);
  }
  const neighborsNonSorted = auxNeighbors(graph, visited, start);
  const neighbors = quickSort(neighborsNonSorted);
  const concatedQueue = flatten([
    ...neighbors.map(
      (element) => ({ a: element, b: start })
    ),
    ...[ ...queue ].slice(1),
  ]);
  const testo = order.concat({ a: queue[0].a, b: queue[0].b });
  const newVisited = visited.concat(start);
  return auxDfs(
    graph,
    newVisited.filter((element, pos) => newVisited.indexOf(element) === pos),
    concatedQueue[0].a,
    concatedQueue,
    testo
  );
}
function dfs(graph, start) {
  return auxDfs(graph, [], start, [ { a: 0, b: -1 } ], []);
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const inputFile = contents.split('\n');
  const graph = inputFile
    .slice(1)
    .map((element) => element.split(' ').map((elm) => parseInt(elm, 10)));
  const solvedObject = dfs(graph, 0, inputFile[0].split(' ')[0] - 1);
  const solvedObjectSorted = quickSortFn(solvedObject, compareMin, compareMax);
  const solvedArray = solvedObjectSorted.map((element) => element.b);
  const output = process.stdout.write(`${ solvedArray.join(' ') }\n`);
  return output;
}

const fileReader = require('fs');
function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node tizcano.js
output:
-1 3 5 18 27 9 12 16 37 10 0 15 33 8 21 1 11 35 2 28 4 7 20 30 31 19 41 6 23 32
8 19 17 14 38 38 33 22 31 17 39 40
*/
