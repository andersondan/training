/*
$ eslint actiradob.js
$
*/

function neighborCount(grid, i, jnn) {
  let neighbor = 0;
  if (grid[i - 1][jnn - 1] === 'X') {
    neighbor += 1;
  }
  if (grid[i - 1][jnn] === 'X') {
    neighbor += 1;
  }
  if (grid[i - 1][jnn + 1] === 'X') {
    neighbor += 1;
  }
  if (grid[i][jnn + 1] === 'X') {
    neighbor += 1;
  }
  if (grid[i + 1][jnn + 1] === 'X') {
    neighbor += 1;
  }
  if (grid[i + 1][jnn] === 'X') {
    neighbor += 1;
  }
  if (grid[i + 1][jnn - 1] === 'X') {
    neighbor += 1;
  }
  if (grid[i][jnn - 1] === 'X') {
    neighbor += 1;
  }
  return neighbor;
}

function turn(grid) {
  const futureGrid = [];
  const max = 60;
  for (let i = 0; i < max; i += 1) {
    const aEmpty = [];
    for (let jnn = 0; jnn < max; jnn += 1) {
      aEmpty.push('-');
    }
    futureGrid.push(aEmpty);
  }
  let cells = 0;
  for (let i = 1; i < max - 1; i += 1) {
    for (let jnn = 1; jnn < max - 1; jnn += 1) {
      const neighbor = neighborCount(grid, i, jnn);
      const three = 3;
      if (neighbor === three) {
        futureGrid[i][jnn] = 'X';
        cells += 1;
      } else if (neighbor === 2 && grid[i][jnn] === 'X') {
        futureGrid[i][jnn] = 'X';
        cells += 1;
      } else {
        futureGrid[i][jnn] = '-';
      }
    }
  }
  return [ futureGrid, cells ];
}

function life(miss, file) {
  const grid = file.split(/\r\n|\r|\n/g);
  for (let i = 0; i < grid.length; i += 1) {
    grid[i] = grid[i].split('');
  }
  let newGrid = [];
  const max = 60;
  for (let i = 0; i < max; i += 1) {
    const aEmpty = [];
    for (let jnn = 0; jnn < max; jnn += 1) {
      aEmpty.push('-');
    }
    newGrid.push(aEmpty);
  }
  const times = 5;
  const middle = 30;
  for (let i = 0; i < grid.length; i += 1) {
    for (let jnn = 0; jnn < grid[i].length; jnn += 1) {
      newGrid[i + middle][jnn + middle] = grid[i][jnn];
    }
  }
  let answer = '';
  for (let i = 0; i < times; i += 1) {
    let cells = 0;
    [ newGrid, cells ] = turn(newGrid);
    answer += `${ cells } `;
  }
  const output = process.stdout.write(answer);
  return output;
}

const fileS = require('fs');

function fileLoad() {
  return fileS.readFile('DATA.lst', 'utf8', (miss, file) => life(miss, file));
}

fileLoad();

/*
$ node actiradob.js
6 5 3 2 0
*/
