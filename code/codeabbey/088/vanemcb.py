# pylint vanemcb.py
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""Solution of challenge 'Pitch and Notes'"""


def main():
    """Function solution"""
    input_data = open("DATA.lst", 'r')
    input_notes = input_data.readline().split()
    input_data.close()

# Matrix that contains the notes of a octave and the number of semitones until
# A4 of each note
    matrix_octave = [['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A',
                      'A#', 'B'], [9, 8, 7, 6, 5, 4, 3, 2, 1, 0, -1, -2]]

    num_notes = len(input_notes)
    vec_notes = []
    vec_num_oct = []
    vec_freq = []

# Cycle to organize the input notes and its octaves
    for i in range(0, num_notes):
        vec_notes.append(input_notes[i][0:len(input_notes[i])-1])
        vec_num_oct.append(int(input_notes[i][len(input_notes[i])-1]))
        vec_freq.append(0)

# Cycles to calculate the frequency of each note
    for k in range(0, num_notes):
        for k_1 in range(0, 12):
            if vec_notes[k] == matrix_octave[0][k_1]:
                num_semi_tones = matrix_octave[1][k_1]+(12*(4-vec_num_oct[k]))
                vec_freq[k] = round(440*((2**(1/12))**(-num_semi_tones)))
    print vec_freq


main()

# python vanemcb.py
# [831, 82, 392, 58, 37, 35, 740, 98, 139, 659, 69, 587, 33, 208, 466, 156,
# 196, 104, 87, 988, 233, 415]
