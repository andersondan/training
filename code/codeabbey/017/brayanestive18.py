#!/usr/bin/env python3
"""
$ pylint brayanestive18.py
$ python brayanestive18.py
Array Checksum
DATA.lst file with array
"""


from functools import reduce as rd


def read_file(name_file):
    """ The function for read file. """
    return open(name_file, 'r').readlines()[1].split(" ")


def calculate_checksum(result=0, element=0):
    """ The function for calculate checksum. """
    result += element
    result *= 113
    result = result % 10000007
    return result


DATA_LIST_INT = list(map(int, read_file("DATA.lst")))

print rd(calculate_checksum, DATA_LIST_INT, 0)
