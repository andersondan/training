/*
With JavaScript - Node.js
Linting:   eslint jenniferagve.js
--------------------------------------------------------------------
✖ 0 problem (0 error, 0 warnings)
*/

function correctVal(element, keyValue) {
/* Change to the correct value for each letter on the sentence*/
  const spaceSim = 32;
  const pointSim = 46;
  const intialPoint = 65;
  const finalPoint = 91;
  const finalValue = element.charCodeAt(0);
  if ((finalValue === spaceSim) || (finalValue === pointSim)) {
    return finalValue;
  } else if ((finalValue - keyValue) < intialPoint) {
    return finalPoint - (intialPoint - (finalValue - keyValue));
  }
  return finalValue - keyValue;
}

function decode(letter, keyValue) {
/* Take every sentence and call the function to decode and translate
it to ASCII*/
  const eachChar = letter.split('');
  const total = eachChar.map((element) => correctVal(element, keyValue));
  const revCode = total.map((element) => String.fromCharCode(element));
  const final = revCode.join('');
  const output = process.stdout.write(`${ final } `);
  return output;
}

function cesarChiper(erro, contents) {
/* Here every function is called to calculate the
decode value of each sentence*/
  if (erro) {
    return erro;
  }
  const inputFile = contents.split('\n');
  const inputSentences = inputFile.slice(1);
  const firstInp = (inputFile[0]).split(' ');
  const keyValue = firstInp.slice(1);
  const decodeVal = inputSentences.map((element) => decode(element, keyValue));
  return decodeVal;
}

const filesRe = require('fs');
function fileLoad() {
/* Load of the file to read it*/
  return filesRe.readFile('DATA.lst', 'utf8', (erro, contents) =>
    cesarChiper(erro, contents));
}
/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
node jenniferagve.js
input data:2 3
YHQL YLGL YLFL.
HYHQ BRX EUXWXV.
-------------------------------------------------------------------
output:
VENI VIDI VICI. EVEN YOU BRUTUS.
*/

