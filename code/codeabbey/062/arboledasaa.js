/*
$ eslint jarboleda.js
$
*/
/*
won't work as functional programing, see:
https://stackoverflow.com/questions/15471291
/sieve-of-eratosthenes-algorithm-in-javascript-running-endless-for-large-number
*/
function eratosthenes(maxNumber) {
  /* Eratosthenes algorithm to find all primes under n*/

  const upperLimit = Math.sqrt(maxNumber);

  /* Make an array from 2 to (maxNumber - 1)*/
  const booleanArray = new Array(maxNumber)
    .fill(true);
  /* starting from 2, 3, 5,... */
  booleanArray[0] = false;
  booleanArray[1] = false;
  /* Remove multiples of primes */
  booleanArray.forEach((value, i) => {
    if (value && i < upperLimit) {
      const innerArray = new Array(Math.ceil(maxNumber / i))
        .fill('a')
        .map((innerValue, innerIndex) => ((i * i) + (innerIndex * i)))
        .filter((innerValue) => (innerValue < maxNumber));
      innerArray.forEach((innerValue) => {
        booleanArray[innerValue] = false;
      });
    }
  });
  const numbers = new Array(maxNumber)
    .fill('a')
    .map((value, index) => (index));
  /* All booleanArray[i] set to true are primes*/
  const output = numbers.filter((value) => (booleanArray[value]));
  return output;
}

const maxNumber = 20000000;
const thePrimes = [ 1 ].concat(eratosthenes(maxNumber));

function solveForNumber(line) {
  const splited = line.split(' ');
  return thePrimes.filter((value) =>
    (splited[0] <= value && value <= splited[1]))
    .length;
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const splited = contents.split('\n').slice(1);
  const answer = splited.map((value) => (solveForNumber(value)))
    .join(' ');
  const output = process.stdout.write(`${ answer }\n`);
  return output;
}

const fileReader = require('fs');

function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node jarboleda.js
output:
4429 21377 25215 39595 1996 4038 3445 23039 11995 16247 10851 5030 9292 8136
 28360 2604 7591 1513 6008 36880 9955 2015 12550 23505 13427 2422

*/
