# $ mix credo
# Checking 3 source files ...
# Analysis took 0.1 seconds (0.03s to load, 0.1s running checks)
# 3 mods/funs, found no issues.
# $ iex
# iex(1)> c("jpverde.ex")

defmodule Sums do
  def sums do
    {:ok, contents} = File.read("DATA.lst")
    nums = contents |> String.split([" ", "\r\n"], trim: true)
    dat = nums |> Enum.map(&String.to_integer/1)
    [amount | vals] = dat
    IO.puts(Enum.sum(vals))
  end
end

# iex(2)> Sums.sums
# 31752
# :ok
