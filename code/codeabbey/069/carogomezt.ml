let t =(Scanf.scanf "%d\n"(fun t ->  t));;


let fib_iter n = let h = ref Big_int.unit_big_int in
                let i = ref Big_int.unit_big_int in
                let j = ref Big_int.zero_big_int in
                let k = ref Big_int.zero_big_int in
                let valAux = ref Big_int.zero_big_int in
                let aux = ref Big_int.zero_big_int in
                while !n > 0 do
                    if ((!n mod 2) <> 0) then
                    begin
                        aux := Big_int.mult_big_int !h !j;
                        valAux := Big_int.add_big_int (Big_int.mult_big_int !h !i) (Big_int.mult_big_int !j !k);
                        j := Big_int.add_big_int !valAux !aux;
                        i := Big_int.add_big_int (Big_int.mult_big_int !i !k) !aux
                    end;
                    aux := Big_int.mult_big_int !h !h;
                    valAux := Big_int.mult_big_int (Big_int.big_int_of_int 2) !h;
                    h :=  Big_int.add_big_int (Big_int.mult_big_int !valAux !k) !aux;
                    k := Big_int.add_big_int (Big_int.mult_big_int !k !k) !aux;
                    n := !n / 2;
                done;
                Big_int.string_of_big_int !j;;

let printArray r = Array.iter (Printf.printf "%s ") r;;
(* let a = fib_iter (ref 9);;
Printf.printf "%s " a;;
10001*)
let fibSeq = Array.make 10001 "h";;
for i=0 to 10000 do
    let f = fib_iter (ref i) in
    fibSeq.(i) <- f
done;

(* printArray fibSeq;; *)

let values = read_line () in
let intlist = List.map int_of_string(Str.split (Str.regexp " ") values) in

for i=0 to (List.length intlist) - 1 do
    let result = ref 0 in
    let n = Big_int.big_int_of_int (List.nth intlist i) in
    let j = ref 1 in
    let divisible = ref true in
    while (!divisible) do
        (* Printf.printf "%d " !j; *)
        let v = Big_int.big_int_of_string fibSeq.(!j) in
        if (Big_int.eq_big_int (Big_int.mod_big_int v n)  Big_int.zero_big_int) then
        (
            result := !j;
            divisible := false
        )
        else (
                j := !j + 1;
                divisible := true;
            )
    done;
    Printf.printf "%d " !result
done;;
