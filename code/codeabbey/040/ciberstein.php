<?php
/*
PHP version 7.2.11.0

Linting with "SublimeLinter-PHP"

phpcs ciberstein.php

Compiling and linking using the "Command Windows prompt"

> C:\\..\php \\..\ciberstein.php
./output
*/
if(file_exists('./DATA.lst')) {
  $data = fopen('./DATA.lst', 'r');
  list($Y,$X) = explode(' ',fgets($data, 128));

  for($v=0;$v<$Y;$v++) {
    $obj = explode(' ',fgets($data, 65536));

    for($i = 0 ; $i < $X ; $i++) {
      if(trim($obj[$i])=='X') {
        continue;
      }
      if($v==0 && $i==0) {
        $M[$v][$i] = 1;
        continue;
      }
      $M[$v][$i] = ($v > 0?$M[$v - 1][$i]:0) + ($i > 0?$M[$v][$i - 1]:0);
    }
  }
  echo $M[$Y-1][$X-1];
}
else
  echo 'Error DATA.lst not found';
/*
./ciberstein.php
3547333
*/
?>
