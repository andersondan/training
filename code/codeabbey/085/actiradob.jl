# $ julia
#
#   _       _ _(_)_     |  A fresh approach to technical computing
#  (_)     | (_) (_)    |  Documentation: https://docs.julialang.org
#   _ _   _| |_  __ _   |  Type "?help" for help.
#  | | | | | | |/ _` |  |
#  | | |_| | | | (_| |  |  Version 0.6.4 (2018-07-09 19:09 UTC)
# _/ |\__'_|_|_|\__'_|  |  Official http://julialang.org/ release
#|__/                   |  x86_64-w64-mingw32
# $ using Lint
# $ length(lintfile("actiradob.jl"))
# 0

function rotate2D()
  answer = ""
  open("DATA.lst") do f
    n, d = split(readline(f))
    n = parse(Int32,n)
    d = parse(Int32,d)
    data = Array{Tuple{String,Float64,Float64}}(n)
    mRot = [[cosd(d), sind(d)] [-sind(d), cosd(d)]]
    for i = 1:n
      name, x, y = split(readline(f))
      x = parse(Int32,x)
      y = parse(Int32,y)
      xn, yn = mRot*[x,y]
      data[i] = (name,xn,yn)
    end
    sort!(data, by = p -> p[3])
    answer = string(data[1][1])
    for i = 2:n
      answer = string(answer, " ", data[i][1])
    end
  end
  return answer
end

rotate2D()

# $ include("actiradob.jl")
# "Diadem Alcor Capella Pherkad Mizar ...
# ... Lesath Deneb Castor Fomalhaut"S
