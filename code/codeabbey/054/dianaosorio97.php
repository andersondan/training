<?php
/**
 *$ phpcs dianaosorio97.php #linting
 *$ php dianaosorio97.php  #Compilation
 *
 */

if (file_exists('DATA.lst')) {
  $file = fopen("DATA.lst", "r");
  $data = file("DATA.lst");
  $data = array_map('intval', $data);
  $b = 0;
  $c = 0;
  for ($i=1; $i<count($data); $i++) {
    $aux = $data[$i];
    for ($j=2; $j < $aux; $j++) {
      if ((((pow($aux, 2))-(2*$aux*$j)) % ((2*$aux)-(2*$j)))==0) {
        $b = ((pow($aux, 2))-(2*$aux*$j))/((2*$aux)-(2*$j));
        $c = pow(($aux-$j-$b),2);
        if ($c>0) {
          echo $c." ";
          break;
        }
      }
    }
  }
  echo "\n";
} else {
    echo "Fail";
}

#$php dianaosorio97.php
#output:
#32066907445225 42720930015625 54497364066289
#67558947330625 95482309965025 52118826719569
#46102158080449 43897316505025 75972229602025
?>
