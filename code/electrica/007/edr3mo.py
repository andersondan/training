# !usr/bin/python
# [Finished in 0.1s]

# pylint edr3mo.py
# No config file found, using default configuration
#
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 6.00/10, +4.00)

"""
Given two variables calculate the final value
"""


def dry_run():
    """
    Print the final value
    """
    data_file = open("DATA.lst", "r")
    values = data_file.read()
    value_x = int(values.split(" ")[0])
    value_y = int(values.split(" ")[1])
    while value_y <= 100:
        value_x = value_x+value_y*value_y+value_y+1
        value_y = value_y+1
    print value_x
# python ./edr3mo.py
# 343500
