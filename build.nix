#!/usr/bin/env nix-shell
#!nix-shell -i bash -p docker gitlab-runner

if [ -z ${DOCKER_USER+x} ]; then echo "DOCKER_USER not set"; exit 1; fi
if [ -z ${DOCKER_PASS+x} ]; then echo "DOCKER_PASS not set"; exit 1; fi

echo "${DOCKER_PASS}" | \
docker login registry.gitlab.com -u "${DOCKER_USER}" --password-stdin

gitlab-runner exec docker checks
gitlab-runner exec docker commitlint
gitlab-runner exec docker scons
