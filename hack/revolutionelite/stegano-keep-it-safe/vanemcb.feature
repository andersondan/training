## Version 2.0
## language: en

Feature: Keep It Safe - Steganography - Revolution Elite
  Site:
    Revolution Elite
  Category:
    Steganography
  User:
    vanem_cb
  Goal:
    Find the combination of the safe

  Background:
  Hacker's software
    | <Software name> |       <Version>      |
    | Windows         | 10.0.17134.407 (x64) |
    | Chrome          | 70.0.3538.77         |
    | Microsoft Paint | 1803                 |
    | ImageHide       | 2.0.0.0              |
    | HexBrowserNET   | 1.1.0.72             |
  Machine information:
    Given the challenge URL
    """
    https://www.sabrefilms.co.uk/revolutionelite/keep-it-safe.php
    """
    Then I open the URL with Google Chrome
    And I see the challenge statement
    """
    You know another vital clue is hidden in the safe...
    But how to find the combination?
    """
    And I see an image
    And I download the image [evidence](originalimage.png)

  Scenario: Fail: Looking for with steganography tool
    Given the image file
    Then I open the file in a steganography tool [evidence](textonimage.png)
    """
    ImageHide.exe
    """
    And I click in "Read Data"
    But nothing happens
    And I can't find the combination
    Then I don't solve the challenge

  Scenario: Fail: Looking for hexadecimal information
    Given the image file
    Then I open the file in a tool file identification
    """
    HexBrowserNET.exe
    """
    And I see the hexadecimal information [evidence](hexinformation.png)
    But I don't find something relevant
    And I can't find the combination
    Then I don't solve the challenge

  Scenario: Succes: Edit image
    Given the image file
    Then I open the file in an image editor
    """
    Microsoft Paint
    """
    And I zoom in the image to find something
    And I invert the colors
    But I can't find something
    Then I use the tool "Fill"
    And I find something [evidence](paintimage.png)
    """
    1000000000-(((72/2)*(8%5))*(148*(28*(16*(8*(4+4+7+2))))))
    """
    And I compute the result
    """
    26126848
    """
    Then I write the combination in the input field to submit the answer
    And I solve the challenge
