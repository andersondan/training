package hashtester;

import java.math.BigInteger;

public class HashTester {

    public static void main(String[] args) {
        //Tanto el número mágico como la semilla son correctos
        Generador gen = new Generador(49374); //Iniciamos el generador con el número mágico
        Generador.setSemilla("wow.sinfocol.org"); //Inicializamos la semilla
        System.out.println(gen.wowHash("World of Wargame hash")); //Imprimimos el hash
    }
}

class Generador {
    static String semilla = "";
    long magicNumber = 0;
    private final BigInteger CERO = BigInteger.ZERO;
    private final BigInteger UNO = BigInteger.ONE;
    private final BigInteger D6 = new BigInteger("16");
    private final static char[] cambExp = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

     public Generador(long magicNumber){
        this.magicNumber = magicNumber;
     }

     public String wowHash(String str){
        long tmp = magicNumber;//tmp = 49374
        str += semilla;  //str = prueba wowHashwow.sinfocol.org
        int i = 0;
        int len = str.length();
        while(i < len)
            tmp += (tmp << 3) + str.codePointBefore(++i);//operación numérica
        return Int2BaseR(BigInteger.valueOf(tmp), D6);
    }

    private String Int2BaseR(BigInteger num, BigInteger base){
        if(num.compareTo(base.subtract(UNO)) < 0){
            return (num.mod(base).compareTo(CERO) == 0)? "": num.mod(base).toString();
        }else{
            return Int2BaseR( num.divide( base ), base) + cambBase( num.mod( base ).toString() );
        }
    }
    private String cambBase(String num){
        return String.valueOf( cambExp[Integer.parseInt(num)] );
    }

    public static String getSemilla() {
        return semilla;
    }

    public static void setSemilla(String semilla) {
        Generador.semilla = semilla;
    }

    public long getMagicNumber() {
        return magicNumber;
    }

    public void setMagicNumber(long magicNumber) {
        this.magicNumber = magicNumber;
    }

}
