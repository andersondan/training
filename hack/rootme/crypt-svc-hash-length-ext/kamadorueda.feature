## Version 2.0
## language: en

Feature: Root Me - Crypto - Hash length extension attack
  Site:
    Root Me
  Category:
    Crypto
  Challenge:
    Hash length extension attack
  User:
    kedavamaru

  Background:
  Hacker's software:
    | <Software name> | <Version>                            |
    | GNU bash        | version 4.4.19 (x86_64-pc-linux-gnu) |
    | Ubuntu          | 18.04.1 LTS (amd64)                  |
    | Google Chrome   | 70.0.3538.77 (64-bit)                |

  Machine information:
    Given I'm accesing the challenge page
    And the problem statement is provided
    """
    Find the flag.
    """
    And a subtitle is provided
    """
    H(key + message)
    """
    And the Challenge connection informations are known
    | Host                    | Protocol | Port  |
    | challenge01.root-me.org | TCP      | 51022 |


  Scenario Outline: Fail: Reconaissance #1, salt and hash
    When I run netcat and enter as inputs <user> and <role>
    Then I get <results>
    And some messages with information are displayed
    And I conclude that the hash algorithm is sha256
    And I conclude that the salt is 16 bytes of length

    Examples:
    | <user> | <role>    | <results>                        |
    | kamado | admin     | Forbidden role !                 |
    | kamado | guest     | A valid token for "kamado:guest" |
    | kamado |           | A valid token for "kamado:"      |

  Scenario: Fail: Reconaissance #2, token format
    Given I have a valid token then my objective is to reverse engineer it
    """
    User:  kamado
    Role:  guest
    Token: a2FtYWRvOmd1ZXN0:MWIxNzAwOTY3NTdkNjM1NDM0YmY5YzkxZmJiZDZiYzFlNDA2MDQx
           ZTI2ZmZlZWE5YjRlODU1NjMxYTZkODIyYg==
    """
    Then I split the token in two parts
    """
    Token format: ${token_before_colon}:${token_after_colon}
    """

  Scenario: Fail: Reconaissance #3, token decoding
    Given I have the same valid token from the previous scenario
    And I recognize "==" as paddings on base64 encoding
    When I decode assumming the token is in base64
    Then I get
    """
    token_before_colon:
      kamado:guest
    token_after_colon:
      1b170096757d635434bf9c91fbbd6bc1e406041e26ffeea9b4e855631a6d822b
    """
    And I conclude that the token is encoded in base64
    And I conclude the token_before_colon is
    """
    username:role
    """
    And I conclude the token_after_colon is (presumably)
    # based on the title H(key + message)
    """
    SHA256(salt + token_before_colon)
    """

  Scenario: Fail: assemble an attack vector (hex-hex)
    Given I get a valid token
    """
    $ nc challenge01.root-me.org 51022
    Select an option :
    0 : Register
    1 : Login
    0
    Enter your login: kamado:
    Enter your role (or guest):
    ...
    a2FtYWRvOjo=:Mjg5ZTVkNDU1NjkyYTA3NTdmYTA3ZTU2ZWQ2ZmY4ZjQ5NmY1YmIxZTU2NTk2MDY
    xZTUwNmQ3OWFlZGNmNDM4OQ==
    """
    When I split the token
    """
    token_before_colon:
      a2FtYWRvOjo=
    token_after_colon:
      Mjg5ZTVkNDU1NjkyYTA3NTdmYTA3ZTU2ZWQ2ZmY4ZjQ5NmY1YmIxZTU2NTk2MDYxZTUwNmQ3OW
      FlZGNmNDM4OQ==
    """
    And I decode the token assumming base64
    """
    token_before_colon:
      kamado::
    token_after_colon:
      289e5d455692a0757fa07e56ed6ff8f496f5bb1e56596061e506d79aedcf4389
    """
    And I perform a hash extension attack with the parameters
    # https://github.com/iagox86/hash_extender
    """
    $ ./hash_extender -d "kamado::" --secret 16 -a "admin" -f sha256 \
    > -s 289e5d455692a0757fa07e56ed6ff8f496f5bb1e56596061e506d79aedcf4389 \
    > --out-data-format=hex --out-signature-format=hex
    Type:
      sha256
    Secret length:
      16
    New signature:
      eb6994dab1cd2b34767a2759e8031a88999176d6e4fc5949a4bf702b7e769f32
    New string:
      6b616d61646f3a3a8000000000000000000000000000000000000000000000000000000000
      00000000000000000000c061646d696e
    """
    # New string:
    #  0000: 6B 61 6D 61 64 6F 3A 3A 80 00 00 00 00 00 00 00    kamado::........
    #  0010: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00    ................
    #  0020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 B8    ................
    #  0030: 61 64 6D 69 6E                                     admin
    And I craft a new token
    """
    token_before_colon:
      6b616d61646f3a3a8000000000000000000000000000000000000000000000000000000000
      00000000000000000000c061646d696e
    token_after_colon:
      eb6994dab1cd2b34767a2759e8031a88999176d6e4fc5949a4bf702b7e769f32
    """
    And I encode the token_before_colon assumming it is in hexadecimal
    And I encode the token_after_colon assumming it is in hexadecimal
    """
    token_before_colon:
      a2FtYWRvOjqAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAYWRtaW4=
    token_after_colon:
      62mU2rHNKzR2eidZ6AMaiJmRdtbk/FlJpL9wK352nzI=
    """
    Then I get a presumably valid token
    """
    Token: ${token_before_colon}:${token_after_colon}
    """
    When I inject this into the service
    Then I get "Bad Token!"

  Scenario: Fail: assemble an attack vector (hex-string)
    Given I get, split, decode, extend and craft a valid token_after_colon
    """
    token_before_colon:
      6b616d61646f3a3a8000000000000000000000000000000000000000000000000000000000
      00000000000000000000c061646d696e
    token_after_colon:
      eb6994dab1cd2b34767a2759e8031a88999176d6e4fc5949a4bf702b7e769f32
    """
    # same as in previous scenario up to this point
    And I encode the token_before_colon assumming it is in hexadecimal
    And I encode the token_after_colon assumming it is an ASCII string
    """
    token_before_colon:
      a2FtYWRvOjqAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAYWRtaW4=
    token_after_colon:
      ZWI2OTk0ZGFiMWNkMmIzNDc2N2EyNzU5ZTgwMzFhODg5OTkxNzZkNmU0ZmM1OTQ5YTRiZjcwMm
      I3ZTc2OWYzMg==
    """
    Then I get a presumably valid token
    """
    Token: ${token_before_colon}:${token_after_colon}
    """
    When I inject this into the service
    Then I get "Unkown role (?)(?)admin"
    And I conclude all I need is to correct the role

  Scenario: Success: assemble the correct attack vector
    Given I get a valid token
    """
    $ nc challenge01.root-me.org 51022
    Select an option :
    0 : Register
    1 : Login
    0
    Enter your login: kamado
    Enter your role (or guest):
    ...
    a2FtYWRvOg==:YTQwYTkyYjdhZWRkYTZjN2M2MGQwYzYzYTEwZjYxOTA0YzQzMWE1MWRjMjU3MjY
    wYzI1ZDY2ODllNDI4MDdkYQ==
    """
    When I split the token
    """
    token_before_colon:
      a2FtYWRvOg==
    token_after_colon:
      YTQwYTkyYjdhZWRkYTZjN2M2MGQwYzYzYTEwZjYxOTA0YzQzMWE1MWRjMjU3MjYwYzI1ZDY2OD
      llNDI4MDdkYQ==
    """
    And I decode the token assumming base64
    """
    token_before_colon:
      kamado:
    token_after_colon:
      a40a92b7aedda6c7c60d0c63a10f61904c431a51dc257260c25d6689e42807da
    """
    And I perform a hash extension attack with the parameters
    # https://github.com/iagox86/hash_extender
    """
    $ ./hash_extender -d "kamado:" --secret 16 -a ":admin" -f sha256 \
    > -s a40a92b7aedda6c7c60d0c63a10f61904c431a51dc257260c25d6689e42807da \
    > --out-data-format=hex --out-signature-format=hex
    Type:
      sha256
    Secret length:
      16
    New signature:
      ceddf77cfe19e54af9222bfc960dd8c6ec4f97e9b9a0a0837158f6b504a07fc1
    New string:
      6b616d61646f3a800000000000000000000000000000000000000000000000000000000000
      00000000000000000000b83a61646d696e
    """
    # New string:
    #  0000: 6B 61 6D 61 64 6F 3A 80 00 00 00 00 00 00 00 00    kamado:.........
    #  0010: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00    ................
    #  0020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 B8    ................
    #  0030: 3A 61 64 6D 69 6E                                  :admin
    And I craft a new token
    """
    token_before_colon:
      6b616d61646f3a800000000000000000000000000000000000000000000000000000000000
      00000000000000000000b83a61646d696e
    token_after_colon:
      ceddf77cfe19e54af9222bfc960dd8c6ec4f97e9b9a0a0837158f6b504a07fc1
    """
    And I encode the token_before_colon assumming it is in hexadecimal
    And I encode the token_after_colon assumming it is a string
    """
    token_before_colon:
      a2FtYWRvOoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC4OmFkbWlu
    token_after_colon:
      Y2VkZGY3N2NmZTE5ZTU0YWY5MjIyYmZjOTYwZGQ4YzZlYzRmOTdlOWI5YTBhMDgzNzE1OGY2Yj
      UwNGEwN2ZjMQ==
    """
    Then I get a presumably valid token
    """
    Token: ${token_before_colon}:${token_after_colon}
    """
    When I inject this into the service
    Then I get "Connected!" response
    """
    $ nc challenge01.root-me.org 51022
    Select an option :
    0 : Register
    1 : Login
    1
    Enter your secret token : a2FtYWRvOoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
    AAAAAAAAAAAAC4OmFkbWlu:Y2VkZGY3N2NmZTE5ZTU0YWY5MjIyYmZjOTYwZGQ4YzZlYzRmOTdlO
    WI5YTBhMDgzNzE1OGY2YjUwNGEwN2ZjMQ==
    Connected !
    """
    And I solve this challenge
    """
    Welcome back! Flag to validate is u$3_HMAC_l4m3rZ!
    """

    # Don't use md5, SHA1, or SHA2 to auth a token based on a padded 'secret'
    # because they are based on the merkle-damgard construction, therefore weak.
    # Use SHA3 or HMAC
