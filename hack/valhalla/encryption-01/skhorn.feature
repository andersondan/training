# language: en

Feature: Solve Encryption challenge 1
  From site Valhalla
  From Encryption Category
  With my username Skhorn

  Background:
    Given the fact i have an account on Valhalla site
    And i have Debian 9 as Operating System
    And i have internet access

  Scenario: Failed attempt
    Given the link to the challenge
    And a crypted text represented as single numbers
    And a question stating: What is the password?
    And i convert the ciphertext to base 2
    When i try to solve the challenge
    And i get that the answer was wrong
    Then i retry the challenge with another base


  Scenario: Succesful Solution
    Given the link to the challenge
    And a crypted text represented as single numbers
    And a question stating: What is the password?
    And i convert the ciphertext to base 3
    When i try to solve the challenge
    And i get that the answer was correct
    Then i solve the challenge
