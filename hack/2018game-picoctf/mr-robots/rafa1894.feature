## Version 2.0
## language: en

Feature: mr-robots-web-exploitation-2018game-picoctf
  Code:
    Mr. Robots
  Site:
    2018game-picoctf
  Category:
    Web Exploitation
  User:
    rferi1894
  Goal:
    Search for the flag hidden in the website.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.2 LTS |
    | Firefox         | 67.0.1      |
    | Burp Suite      | 1.7.36      |

  Machine information:
    Given the challenge URL
    """
    http://2018shell.picoctf.com:40064
    """
    And the challenge information
    """
    Do you see the same things I see?
    The glimpses of the flag hidden away?
    """"
    And the field to submit the flag

  Scenario: Fail:code-source-and-google-dorks
    When I access the site http://2018shell.picoctf.com:40064
    Then I begin searching around the page
    And I also inspect all the source code
    Then I conclude the main page won't help me find the flag
    And I check if the site has more pages with google dorks
    And google dorks isn't able to find any more pages for the site
    And I conclude that I need to use a better tool

  Scenario: Success:web-crawling
    When I access the site http://2018shell.picoctf.com:40064
    Then I use Burp Suite to intercept the page responses
    And I don't see anything suspicious on them
    Then I try using the Burp Suite web crawler on the website
    And it finds a robots.txt along with the hidden web page
    And I inspect the response for the hidden web page
    And I catch the flag
