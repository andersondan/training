#lenguage: en

Feature: Solve PHP 0815 challenge
  From the WeChall site
  Of the category Exploit and PHP
  As the registered user disassembly

  Background:
    Given a PHP script prone to SQL injection
    And some hints posted in the forum

  Scenario Outline: Provide a fix for the SQL injection vulnerability

  Scenario: Identify where the vulnerability is
    Given the PHP code, I found the main problem in the query
    """
    12  $query = "SELECT 1 FROM `table` WHERE `id`=$show";
    """

  Scenario: Search for hints in the forum
    Given the first post where someone mention the word typecasting
    Given I look out for the ways to typecast a string into a integer on PHP
    And test one of the shortest way to do it in writephponline.com

  Scenario: Provide the solution
    Given the solution box, I type the answer
    """
    +0
    """
    And solve the challenge
