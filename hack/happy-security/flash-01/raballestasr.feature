# language: en

Feature: Solve the challenge mini-flash
  From the happy-security.de website
  From the Flash category
  With my username raballestas

  Background:
    Given a flash animation

  Scenario: Succesful solution
    When I look into the page source code
    And curl the swf file
    And decompile it with ffdec
    And I look at the swf's code
    Then I see that the input must be 12 characters long
    And I get some of the letters
    And I fill in the rest by intution
    Then I solve the challenge
