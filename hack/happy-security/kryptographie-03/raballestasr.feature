# language: en

Feature: Solve the challenge Baphomet
  From the happy-security.de website
  From the Kryptographie category
  With my username raballestas

  Background:
    Given a string

  Scenario: Successful solution
    When I decode the string using atbash
    Then I see the password is contained in the decoded string
    And I solve the challenge
